<?php
$tipo = $_GET["tipo"] ?? null;
$database = "sagamedi_sats_machado";
$table = "ponto as p right join supervisorio as sup on sup.ponto_id = p.id_ponto";
$columns = "sup.id, p.id_ponto, p.ponto_tipo_id, p.nome_ponto, p.max_reservatorio, sup.value, sup.sup_left, sup.sup_top, sup.type, sup.variable, sup.width, sup.height, sup.sup_angle";
$where = isset($tipo) ? "sup.tabcontrol='$tipo' group by sup.id" : null;
header('Content-type:application/json;charset=utf-8');
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
$con = mysqli_connect("server-machado.softether.net", "vuejs", "vuejs@saga*245", $database, "9989") ?? die();
mysqli_set_charset($con, "UTF8") ?? die();
$query = "SELECT {$columns} FROM {$table}";
if (!empty($where) && $where != null) {
	$query .= " WHERE {$where}";
}

$result = mysqli_query($con, $query);
if($result === false)
	echo $result;

$output = array();
while ($row = mysqli_fetch_assoc($result))
	array_push($output, $row);

if (count($output) > 0) {
	echo json_encode($output);
}
mysqli_close($con);
?>