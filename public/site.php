<?php
$css = <<<CSS3
<style>
    .distribuicao { z-index: 1; }
    .componente {
    width: 100px;
    height: 200px;
    position: absolute;
    cursor: move;
    z-index: 100;
    /* border: 1px solid black; */
    }
    .reservatorio {
    position: relative;
    /* top: 50%;
    left: 50%;
    height: 100px;
    transform: translate(-50%, -50%); */
    border: 5px solid #000;
    border-top: 1px solid transparent;
    border-radius: 10px;
    border-top-left-radius: 1px;
    border-top-right-radius: 1px;
    background-position: top right;
    background-repeat: repeat-x;
    background-color: white;
    z-index: 0;
    overflow: hidden;
    }
    .water {
    /* background-image: url("../assets/waves.png"); */
    background-color: #9cc6ff;
    background-position: top right;
    position: absolute;
    bottom: 0px;
    width: 100%;
    height: 50%;
    -webkit-transition: all 3s ease-out;
    -moz-transition: all 3s ease-out;
    transition: all 3s ease-out;
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    border-radius: 2px;
    z-index: -1;
    overflow: hidden;
    }
    .valor-res {
    position: relative;
    font-size: 23px;
    margin-bottom: -15px;
    text-align: center;
    font-weight: bolder;
    z-index: 1;
    }
    .nivel-res-img {
    height: 100%;
    padding: 2;
    position: absolute;
    padding-left: 1px;
    padding-bottom: 2px;
    z-index: 1;
    }
    .nome-res {
    margin: 0;
    padding: 1px;
    padding-top: 35%;
    text-align: center;
    font-weight: bolder;
    font-size: 13px;
    z-index: 1;
    }
    .bubbleArea {
    width: 100%;
    height: 100%;
    margin: 0 auto;
    position: relative;
    }
    @-webkit-keyframes bluePulse {
    from {
        -webkit-box-shadow: 0 0 30px #54a9ed;
    }
    50% {
        -webkit-box-shadow: 0 0 80px #69abdf;
    }
    to {
        -webkit-box-shadow: 0 0 30px #54a9ed;
    }
    }
    @keyframes bubbleFloat1 {
    from {
        top: 89%;
        transform: scale(0.9);
        opacity: 0;
        animation-timing-function: ease-in-out;
    }
    1% {
        bottom: 10%;
        transform: scale(0.3);
        opacity: 0;
        animation-timing-function: ease-in-out;
    }
    30% {
        bottom: 20%;
        transform: scale(0.8);
        opacity: 1;
        animation-timing-function: ease-in-out;
    }
    95% {
        bottom: 85%;
        transform: scale(0.3);
        opacity: 1;
        animation-timing-function: ease-in-out;
    }
    99% {
        bottom: 80%;
        transform: scale(1);
        opacity: 0;
        animation-timing-function: ease-in-out;
    }
    to {
        top: 0%;
        transform: scale(0);
        opacity: 0;
        animation-timing-function: ease-in-out;
    }
    }
    .bubbleArea span.glow {
    width: 10%;
    height: 10%;
    position: relative;
    display: block;
    border-radius: 200px;
    animation-name: bluePulse;
    -webkit-animation-name: bluePulse;
    -moz-animation-name: bluePulse;
    -o-animation-name: bluePulse;
    animation-duration: 2s;
    -webkit-animation-duration: 2s;
    -moz-animation-duration: 2s;
    -o-animation-duration: 2s;
    animation-iteration-count: infinite;
    -webkit-animation-iteration-count: infinite;
    -moz-animation-iteration-count: infinite;
    -o-animation-iteration-count: infinite;
    }
    .bubbleArea span.bubble {
    display: block;
    position: relative;
    background: #fff;
    background: -moz-radial-gradient(
        center,
        ellipse cover,
        rgba(105, 171, 223, 0.2) 0%,
        #54a9ed 100%
    );
    /* FF3.6+ */
    background: -webkit-gradient(
        radial,
        center center,
        0px,
        center center,
        100%,
        color-stop(0%, rgba(105, 171, 223, 0.2)),
        color-stop(100%, #54a9ed)
    );
    /* Chrome,Safari4+ */
    background: -webkit-radial-gradient(
        center,
        ellipse cover,
        rgba(105, 171, 223, 0.2) 0%,
        #54a9ed 100%
    );
    /* Chrome10+,Safari5.1+ */
    background: -o-radial-gradient(
        center,
        ellipse cover,
        rgba(105, 171, 223, 0.2) 0%,
        #54a9ed 100%
    );
    /* Opera 12+ */
    background: -ms-radial-gradient(
        center,
        ellipse cover,
        rgba(105, 171, 223, 0.2) 0%,
        #54a9ed 100%
    );
    /* IE10+ */
    background: radial-gradient(
        ellipse at center,
        rgba(105, 171, 223, 0.2) 0%,
        #54a9ed 100%
    );
    /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#3369abdf", endColorstr="#54a9ed",GradientType=1 );
    /* IE6-9 fallback on horizontal gradient */
    width: 20%;
    height: 20%;
    border-radius: 50%;
    position: relative;
    /* left: random(75) px; */
    /* bottom: 1px; */
    animation-name: bubbleFloat1;
    -webkit-animation-name: bubbleFloat1;
    -moz-animation-name: bubbleFloat1;
    -o-animation-name: bubbleFloat1;
    animation-duration: 5s;
    -webkit-animation-duration: 5s;
    -moz-animation-duration: 5s;
    -o-animation-duration: 5s;
    animation-iteration-count: infinite;
    -webkit-animation-iteration-count: infinite;
    -moz-animation-iteration-count: infinite;
    -o-animation-iteration-count: infinite;
    }
    .bubbleArea span.bubble2 {
    display: block;
    position: relative;
    background: #fff;
    background: -moz-radial-gradient(
        center,
        ellipse cover,
        rgba(105, 171, 223, 0.2) 0%,
        #54a9ed 100%
    );
    /* FF3.6+ */
    background: -webkit-gradient(
        radial,
        center center,
        0px,
        center center,
        100%,
        color-stop(0%, rgba(105, 171, 223, 0.2)),
        color-stop(100%, #54a9ed)
    );
    /* Chrome,Safari4+ */
    background: -webkit-radial-gradient(
        center,
        ellipse cover,
        rgba(105, 171, 223, 0.2) 0%,
        #54a9ed 100%
    );
    /* Chrome10+,Safari5.1+ */
    background: -o-radial-gradient(
        center,
        ellipse cover,
        rgba(105, 171, 223, 0.2) 0%,
        #54a9ed 100%
    );
    /* Opera 12+ */
    background: -ms-radial-gradient(
        center,
        ellipse cover,
        rgba(105, 171, 223, 0.2) 0%,
        #54a9ed 100%
    );
    /* IE10+ */
    background: radial-gradient(
        ellipse at center,
        rgba(105, 171, 223, 0.2) 0%,
        #54a9ed 100%
    );
    /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#3369abdf", endColorstr="#54a9ed",GradientType=1 );
    /* IE6-9 fallback on horizontal gradient */
    width: 20%;
    height: 20%;
    border-radius: 50%;
    position: absolute;
    left: 75%;
    /* bottom: 1px; */
    animation-name: bubbleFloat1;
    -webkit-animation-name: bubbleFloat1;
    -moz-animation-name: bubbleFloat1;
    -o-animation-name: bubbleFloat1;
    animation-duration: 4.2s;
    -webkit-animation-duration: 4.2s;
    -moz-animation-duration: 4.2s;
    -o-animation-duration: 4.2s;
    animation-iteration-count: infinite;
    -webkit-animation-iteration-count: infinite;
    -moz-animation-iteration-count: infinite;
    -o-animation-iteration-count: infinite;
    }
    .bubbleArea span.bubble3 {
    display: block;
    position: relative;
    background: #fff;
    background: -moz-radial-gradient(
        center,
        ellipse cover,
        rgba(105, 171, 223, 0.2) 0%,
        #54a9ed 100%
    );
    /* FF3.6+ */
    background: -webkit-gradient(
        radial,
        center center,
        0px,
        center center,
        100%,
        color-stop(0%, rgba(105, 171, 223, 0.2)),
        color-stop(100%, #54a9ed)
    );
    /* Chrome,Safari4+ */
    background: -webkit-radial-gradient(
        center,
        ellipse cover,
        rgba(105, 171, 223, 0.2) 0%,
        #54a9ed 100%
    );
    /* Chrome10+,Safari5.1+ */
    background: -o-radial-gradient(
        center,
        ellipse cover,
        rgba(105, 171, 223, 0.2) 0%,
        #54a9ed 100%
    );
    /* Opera 12+ */
    background: -ms-radial-gradient(
        center,
        ellipse cover,
        rgba(105, 171, 223, 0.2) 0%,
        #54a9ed 100%
    );
    /* IE10+ */
    background: radial-gradient(
        ellipse at center,
        rgba(105, 171, 223, 0.2) 0%,
        #54a9ed 100%
    );
    /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#3369abdf", endColorstr="#54a9ed",GradientType=1 );
    /* IE6-9 fallback on horizontal gradient */
    width: 20%;
    height: 20%;
    border-radius: 50%;
    position: absolute;
    left: 50%;
    /* bottom: 1px; */
    animation-name: bubbleFloat1;
    -webkit-animation-name: bubbleFloat1;
    -moz-animation-name: bubbleFloat1;
    -o-animation-name: bubbleFloat1;
    animation-duration: 4.25s;
    -webkit-animation-duration: 4.25s;
    -moz-animation-duration: 4.25s;
    -o-animation-duration: 4.25s;
    animation-iteration-count: infinite;
    -webkit-animation-iteration-count: infinite;
    -moz-animation-iteration-count: infinite;
    -o-animation-iteration-count: infinite;
    }
    .bubbleArea span.bubble4 {
    display: block;
    position: relative;
    background: #fff;
    background: -moz-radial-gradient(
        center,
        ellipse cover,
        rgba(105, 171, 223, 0.2) 0%,
        #54a9ed 100%
    );
    /* FF3.6+ */
    background: -webkit-gradient(
        radial,
        center center,
        0px,
        center center,
        100%,
        color-stop(0%, rgba(105, 171, 223, 0.2)),
        color-stop(100%, #54a9ed)
    );
    /* Chrome,Safari4+ */
    background: -webkit-radial-gradient(
        center,
        ellipse cover,
        rgba(105, 171, 223, 0.2) 0%,
        #54a9ed 100%
    );
    /* Chrome10+,Safari5.1+ */
    background: -o-radial-gradient(
        center,
        ellipse cover,
        rgba(105, 171, 223, 0.2) 0%,
        #54a9ed 100%
    );
    /* Opera 12+ */
    background: -ms-radial-gradient(
        center,
        ellipse cover,
        rgba(105, 171, 223, 0.2) 0%,
        #54a9ed 100%
    );
    /* IE10+ */
    background: radial-gradient(
        ellipse at center,
        rgba(105, 171, 223, 0.2) 0%,
        #54a9ed 100%
    );
    /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#3369abdf", endColorstr="#54a9ed",GradientType=1 );
    /* IE6-9 fallback on horizontal gradient */
    width: 20%;
    height: 20%;
    border-radius: 50%;
    position: absolute;
    left: 25%;
    /* bottom: 1px; */
    animation-name: bubbleFloat1;
    -webkit-animation-name: bubbleFloat1;
    -moz-animation-name: bubbleFloat1;
    -o-animation-name: bubbleFloat1;
    animation-duration: 4.5s;
    -webkit-animation-duration: 4.5s;
    -moz-animation-duration: 4.5s;
    -o-animation-duration: 4.5s;
    animation-iteration-count: infinite;
    -webkit-animation-iteration-count: infinite;
    -moz-animation-iteration-count: infinite;
    -o-animation-iteration-count: infinite;
    }
    .componenteCano {
    width: 100px;
    height: 13px;
    position: absolute;
    cursor: move;
    }
    .cano {
    width: 100%;
    height: 100%;
    border: 1px solid black;
    /* border-top: 1px solid black; */
    border-radius: 2px;
    border-top-left-radius: 3px;
    border-top-right-radius: 3px;
    background-position: top right;
    background-repeat: repeat-x;
    z-index: -100;
    }
    .waterCano {
    width: 100%;
    height: 100%;
    /* background-color: skyblue; */
    position: absolute;
    bottom: 0;
    z-index: -100;
    }
    .waterCano {
    /* background-image: url("../assets/waves.png"); */
    background-color: #9cc6ff;
    background-position: top right;
    position: absolute;
    bottom: 0px;
    width: 80%;
    -webkit-transition: all 3s ease-out;
    -moz-transition: all 3s ease-out;
    transition: all 3s ease-out;
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    border-radius: 2px;
    /* z-index: -1000; */
    }
    .bubbleAreaCano {
    width: 100%;
    height: 100%;
    margin: 0 auto;
    position: relative;
    }
    /* SASS */
    /* BEAKER AND BUBBLES */
    @-webkit-keyframes bluePulseCano {
    from {
        -webkit-box-shadow: 0 0 30px #54a9ed;
    }
    50% {
        -webkit-box-shadow: 0 0 80px #69abdf;
    }
    to {
        -webkit-box-shadow: 0 0 30px #54a9ed;
    }
    }
    @keyframes bubbleFloatCano1 {
    from {
        right: 89%;
        transform: scale(0.9);
        opacity: 0;
        animation-timing-function: ease-in-out;
    }
    1% {
        left: 10%;
        transform: scale(0.3);
        opacity: 0;
        animation-timing-function: ease-in-out;
    }
    30% {
        left: 20%;
        transform: scale(0.8);
        opacity: 1;
        animation-timing-function: ease-in-out;
    }
    95% {
        left: 85%;
        transform: scale(0.3);
        opacity: 1;
        animation-timing-function: ease-in-out;
    }
    99% {
        left: 80%;
        transform: scale(1);
        opacity: 0;
        animation-timing-function: ease-in-out;
    }
    to {
        left: 0%;
        transform: scale(0);
        opacity: 0;
        animation-timing-function: ease-in-out;
    }
    }
    .bubbleArea span.glow {
    width: 10%;
    height: 10%;
    position: relative;
    display: block;
    border-radius: 200px;
    animation-name: bluePulseCano;
    -webkit-animation-name: bluePulseCano;
    -moz-animation-name: bluePulseCano;
    -o-animation-name: bluePulseCano;
    animation-duration: 2s;
    -webkit-animation-duration: 2s;
    -moz-animation-duration: 2s;
    -o-animation-duration: 2s;
    animation-iteration-count: infinite;
    -webkit-animation-iteration-count: infinite;
    -moz-animation-iteration-count: infinite;
    -o-animation-iteration-count: infinite;
    }
    .bubbleAreaCano span.bubbleCano {
    display: block;
    position: relative;
    background: #fff;
    background: -moz-radial-gradient(
        center,
        ellipse cover,
        rgba(43, 54, 63, 0.2) 0%,
        #54a9ed 100%
    );
    /* FF3.6+ */
    background: -webkit-gradient(
        radial,
        center center,
        0px,
        center center,
        100%,
        color-stop(0%, rgba(105, 171, 223, 0.2)),
        color-stop(100%, #54a9ed)
    );
    /* Chrome,Safari4+ */
    background: -webkit-radial-gradient(
        center,
        ellipse cover,
        rgba(105, 171, 223, 0.2) 0%,
        #54a9ed 100%
    );
    /* Chrome10+,Safari5.1+ */
    background: -o-radial-gradient(
        center,
        ellipse cover,
        rgba(105, 171, 223, 0.2) 0%,
        #54a9ed 100%
    );
    /* Opera 12+ */
    background: -ms-radial-gradient(
        center,
        ellipse cover,
        rgba(105, 171, 223, 0.2) 0%,
        #54a9ed 100%
    );
    /* IE10+ */
    background: radial-gradient(
        ellipse at center,
        rgba(105, 171, 223, 0.2) 0%,
        #54a9ed 100%
    );
    /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#3369abdf", endColorstr="#54a9ed",GradientType=1 );
    /* IE6-9 fallback on horizontal gradient */
    width: 10px;
    height: 10px;
    border-radius: 100%;
    bottom: 5%;
    position: relative;
    animation-name: bubbleFloatCano1;
    -webkit-animation-name: bubbleFloatCano1;
    -moz-animation-name: bubbleFloatCano1;
    -o-animation-name: bubbleFloatCano1;
    animation-duration: 5s;
    -webkit-animation-duration: 5s;
    -moz-animation-duration: 5s;
    -o-animation-duration: 5s;
    animation-iteration-count: infinite;
    -webkit-animation-iteration-count: infinite;
    -moz-animation-iteration-count: infinite;
    -o-animation-iteration-count: infinite;
    }
    .bubbleAreaCano span.bubble2Cano {
    display: block;
    position: relative;
    background: #fff;
    background: -moz-radial-gradient(
        center,
        ellipse cover,
        rgba(43, 54, 63, 0.2) 0%,
        #54a9ed 100%
    );
    /* FF3.6+ */
    background: -webkit-gradient(
        radial,
        center center,
        0px,
        center center,
        100%,
        color-stop(0%, rgba(105, 171, 223, 0.2)),
        color-stop(100%, #54a9ed)
    );
    /* Chrome,Safari4+ */
    background: -webkit-radial-gradient(
        center,
        ellipse cover,
        rgba(105, 171, 223, 0.2) 0%,
        #54a9ed 100%
    );
    /* Chrome10+,Safari5.1+ */
    background: -o-radial-gradient(
        center,
        ellipse cover,
        rgba(105, 171, 223, 0.2) 0%,
        #54a9ed 100%
    );
    /* Opera 12+ */
    background: -ms-radial-gradient(
        center,
        ellipse cover,
        rgba(105, 171, 223, 0.2) 0%,
        #54a9ed 100%
    );
    /* IE10+ */
    background: radial-gradient(
        ellipse at center,
        rgba(105, 171, 223, 0.2) 0%,
        #54a9ed 100%
    );
    /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#3369abdf", endColorstr="#54a9ed",GradientType=1 );
    /* IE6-9 fallback on horizontal gradient */
    width: 4px;
    height: 4px;
    bottom: 70%;
    border-radius: 200px;
    position: relative;
    animation-name: bubbleFloatCano1;
    -webkit-animation-name: bubbleFloatCano1;
    -moz-animation-name: bubbleFloatCano1;
    -o-animation-name: bubbleFloatCano1;
    animation-duration: 4.8s;
    -webkit-animation-duration: 4.8s;
    -moz-animation-duration: 4.8s;
    -o-animation-duration: 4.8s;
    animation-iteration-count: infinite;
    -webkit-animation-iteration-count: infinite;
    -moz-animation-iteration-count: infinite;
    -o-animation-iteration-count: infinite;
    }
    .bubbleAreaCano span.bubble3Cano {
    display: block;
    position: relative;
    background: #fff;
    background: -moz-radial-gradient(
        center,
        ellipse cover,
        rgba(43, 54, 63, 0.2) 0%,
        #54a9ed 100%
    );
    /* FF3.6+ */
    background: -webkit-gradient(
        radial,
        center center,
        0px,
        center center,
        100%,
        color-stop(0%, rgba(105, 171, 223, 0.2)),
        color-stop(100%, #54a9ed)
    );
    /* Chrome,Safari4+ */
    background: -webkit-radial-gradient(
        center,
        ellipse cover,
        rgba(105, 171, 223, 0.2) 0%,
        #54a9ed 100%
    );
    /* Chrome10+,Safari5.1+ */
    background: -o-radial-gradient(
        center,
        ellipse cover,
        rgba(105, 171, 223, 0.2) 0%,
        #54a9ed 100%
    );
    /* Opera 12+ */
    background: -ms-radial-gradient(
        center,
        ellipse cover,
        rgba(105, 171, 223, 0.2) 0%,
        #54a9ed 100%
    );
    /* IE10+ */
    background: radial-gradient(
        ellipse at center,
        rgba(105, 171, 223, 0.2) 0%,
        #54a9ed 100%
    );
    /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="#3369abdf", endColorstr="#54a9ed",GradientType=1 );
    /* IE6-9 fallback on horizontal gradient */
    bottom: 70%;
    width: 5px;
    height: 5px;
    border-radius: 200px;
    position: relative;
    animation-name: bubbleFloatCano1;
    -webkit-animation-name: bubbleFloatCano1;
    -moz-animation-name: bubbleFloatCano1;
    -o-animation-name: bubbleFloatCano1;
    animation-duration: 3.5s;
    -webkit-animation-duration: 3.5s;
    -moz-animation-duration: 2.5s;
    -o-animation-duration: 3.5s;
    animation-iteration-count: infinite;
    -webkit-animation-iteration-count: infinite;
    -moz-animation-iteration-count: infinite;
    -o-animation-iteration-count: infinite;
    }
    .component {
    position: absolute;
    height: 100px;
    width: 100px;
    cursor: move;
    z-index: 200;
    }
    .nome-bomba {
    margin: 0;
    padding: 1px;
    text-align: center;
    font-weight: bolder;
    font-size: 13px;
    z-index: 1;
    }
    .componenteBombaPoco {
    position: absolute;
    height: 100px;
    width: 100px;
    cursor: move;
    z-index: 200;
    }
    .nome-bombaPoco {
    margin: 0;
    padding: 1px;
    text-align: center;
    font-weight: bolder;
    font-size: 13px;
    z-index: 1;
    }
    .box {
    position: absolute;
    display: table;
    }
    .box p {
    text-align: center;
    vertical-align: middle;
    display: table-cell;
    }
    .componenteJuncao {
    position: absolute;
    height: 100px;
    width: 100px;
    cursor: move;
    z-index: 200;
    }
</style>
CSS3;


$database = "sagamedi_sats_machado";
$table = "ponto as p right join supervisorio as sup on sup.ponto_id = p.id_ponto";
$columns = "sup.id, p.id_ponto, p.ponto_tipo_id, p.nome_ponto, p.max_reservatorio, sup.value, sup.sup_left, sup.sup_top, sup.type, sup.variable, sup.width, sup.height, sup.sup_angle";
$where = "sup.tabcontrol=\"tabPage1\" group by sup.id";
$con = mysqli_connect("server-machado.softether.net", "vuejs", "vuejs@saga*245", $database, "9989") ?? die();
mysqli_set_charset($con, "UTF8") ?? die();
$query = "SELECT {$columns} FROM {$table}";
if (!empty($where) && $where != null) {
	$query .= " WHERE {$where}";
}

$result = mysqli_query($con, $query);
if($result === false)
	echo $result;

$pontos = array();
while ($row = mysqli_fetch_assoc($result))
	array_push($output, $row);

mysqli_close($con);

function Reservatorio($id, $max, $res, $nome, $top, $left, $width, $height, $angle, $type){
    $value = floor(($res * 100) / $max);
    $tipo = $type == 'TANQUE' ? 
        '<p class="valor-res">
            '.$value.' %
        </p>' 
        : 
        '<p class="valor-res"></p>';
    $reservatorio = '
        <div style="left: '.$left.'px; top: '.$top.'px; width: '.$width.'px; height: '.$height.'px; transform: rotate('.$angle.'deg);"
            class="componente"
            id="'.$id.'">
            '.$tipo.'
            <br />
            <div class="reservatorio" id="'.$id.'-res">
                <div class="water" style="height:'.$value.'%;">            
                    <div class="bubbleArea">
                        <span class="bubble">
                        <span class="glow"> </span>
                        </span>
                        <span class="bubble2">
                        <span class="glow"> </span>
                        </span>
                        <span class="bubble3">
                        <span class="glow"> </span>
                        </span>
                        <span class="bubble4">
                        <span class="glow"> </span>
                        </span>
                    </div>
                </div>
                <p class="nome-res">'.$nome.'</p>
            </div>
        </div>
    ';

    echo $reservatorio;
}

function Bomba($id, $max, $res, $nome, $top, $left, $width, $height, $angle, $type){
    $bomba = '
        <div style="left: '.$left.'px; top: '.$top.'px; width: '.$width.'px; height: '.$height.'px; transform: rotate('.$angle.'deg);"
            class="componente"
            id="'.$id.'">
        </div>
    ';
    echo $bomba;
}

function BombaPoco($id, $max, $res, $nome, $top, $left, $width, $height, $angle, $type) {
    $bombaPoco = '
        <div style="left: '.$left.'px; top: '.$top.'px; width: '.$width.'px; height: '.$height.'px; transform: rotate('.$angle.'deg);"
            class="component"
            id="'.$id.'">
        </div>
    ';
    echo $bombaPoco;
}

function Cano($id, $max, $res, $nome, $top, $left, $width, $height, $angle, $type){
    $animate = $res > 0 ? 
        '<div class="waterCano" style="`width: 100%;`">
            <div class="bubbleAreaCano">
                <span class="bubbleCano">
                    <span class="glowCano"> </span>
                </span>
                <span class="bubble2Cano">
                    <span class="glowCano"> </span>
                </span>
                <span class="bubble3Cano">
                    <span class="glowCano"> </span>
                </span>
            </div>
        </div>'
        : 
        '<div class="waterCano" style="width: 0%;"></div>';
    $cano = '
        <div style="left: '.$left.'px; top: '.$top.'px; width: '.$width.'px; height: '.$height.'px; transform: rotate('.$angle.'deg);"
            class="componenteCano"
            id="'.$id.'">
            <div class="cano">
                '.$animate.'
            </div>
        </div>
    ';
    echo $cano;
}

function Juncao($id, $max, $res, $nome, $top, $left, $width, $height, $angle, $type){
    $svg = '';
    if($type == 'JUNCAOL')
        $svg = '
        <div class="center">
            <svg
                xmlns:dc="http://purl.org/dc/elements/1.1/"
                xmlns:cc="http://creativecommons.org/ns#"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:svg="http://www.w3.org/2000/svg"
                xmlns="http://www.w3.org/2000/svg"
                xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
                xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
                width="'.$width.'"
                height="'.$height.'"
                viewBox="0 0 100 100"
                version="1.1"
                id="'.$id.'-svg-juncao"
                inkscape:version="0.92.4 (5da689c313, 2019-01-14)"
                sodipodi:docname="cuved_union_cano_sats.svg"
            >
                <defs id="defs5359">
                <linearGradient
                    gradientTransform="matrix(1.3211704,0,0,1.2681309,-171.18085,135.16106)"
                    inkscape:collect="always"
                    xlink:href="#linearGradient4683"
                    id="linearGradient4685"
                    x1="198.0585"
                    y1="57.863068"
                    x2="164.50136"
                    y2="90.114868"
                    gradientUnits="userSpaceOnUse"
                    spreadMethod="reflect"
                />
                <linearGradient inkscape:collect="always" id="linearGradient4683">
                    <stop
                    style="stop-color:#0669b2;stop-opacity:1;"
                    offset="0"
                    id="stop4679"
                    />
                    <stop
                    style="stop-color:#00779f;stop-opacity:1"
                    offset="1"
                    id="stop4681"
                    />
                </linearGradient>
                </defs>
                <sodipodi:namedview
                id="base"
                pagecolor="#ffffff"
                bordercolor="#666666"
                borderopacity="1.0"
                inkscape:pageopacity="0.0"
                inkscape:pageshadow="2"
                inkscape:zoom="0.35"
                inkscape:cx="7.7662276"
                inkscape:cy="559.70892"
                inkscape:document-units="mm"
                inkscape:current-layer="layer1"
                showgrid="false"
                inkscape:window-width="1366"
                inkscape:window-height="705"
                inkscape:window-x="-8"
                inkscape:window-y="79"
                inkscape:window-maximized="1"
                />
                <metadata id="metadata5362">
                <rdf:RDF>
                    <cc:Work rdf:about="">
                    <dc:format>image/svg+xml</dc:format>
                    <dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
                    <dc:title></dc:title>
                    </cc:Work>
                </rdf:RDF>
                </metadata>
                <g
                inkscape:label="Camada 1"
                inkscape:groupmode="layer"
                id="layer1"
                transform="translate(0,-197)"
                >
                <path
                    style="opacity:1;vector-effect:none;fill:url(#linearGradient4685);fill-opacity:1;stroke:none;stroke-width:0.64718956;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                    d="m 2e-6,197.00005 v 55.48242 H 6.60341 v -3.04398 h 24.768894 14.781174 v 17.49522 23.728 h -3.16104 V 297 h 57.00756 v -6.33829 h -2.91117 v -53.68618 -11.98263 -6.6e-4 c 0,-0.42322 -0.0107,-0.84371 -0.0328,-1.26151 -0.0877,-1.67144 -0.34849,-3.29715 -0.76601,-4.86184 -0.1044,-0.39113 -0.21908,-0.77805 -0.34274,-1.16124 -0.24738,-0.7666 -0.53198,-1.5166 -0.85342,-2.24843 -0.16064,-0.36581 -0.33094,-0.72688 -0.50931,-1.08325 -0.71356,-1.42552 -1.56647,-2.77336 -2.54251,-4.02762 -0.4879,-0.62698 -1.0067,-1.23055 -1.5539,-1.8087 v -6.5e-4 c -0.27367,-0.28915 -0.55479,-0.57185 -0.84248,-0.848 -0.86302,-0.82837 -1.78794,-1.59697 -2.76782,-2.29953 h -7e-4 c -0.65319,-0.46831 -1.33055,-0.90788 -2.03043,-1.31523 h -6.9e-4 c -0.69991,-0.40736 -1.4219,-0.78347 -2.16428,-1.12587 h -7e-4 c -1.85625,-0.85602 -3.83865,-1.50258 -5.91453,-1.9083 h -6.9e-4 c -1.24546,-0.24338 -2.52454,-0.40016 -3.83015,-0.46331 h -6.9e-4 c -0.4353,-0.0211 -0.87335,-0.0315 -1.31427,-0.0315 H 58.637188 31.372224 6.60333 V 197 Z"
                    id="rect4637"
                    inkscape:connector-curvature="0"
                    inkscape:label="curved_union"
                />
                </g>
            </svg>
        </div>';
    else if($type == 'JUNCAOT')
        $svg = '
        <div class="center">
            <svg
                xmlns:dc="http://purl.org/dc/elements/1.1/"
                xmlns:cc="http://creativecommons.org/ns#"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:svg="http://www.w3.org/2000/svg"
                xmlns="http://www.w3.org/2000/svg"
                xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
                xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
                width="'.$width.'"
                height="'.$height.'"
                viewBox="0 0 100 70"
                version="1.1"
                id="'.$id.'-svg-juncao"
                inkscape:version="0.92.4 (5da689c313, 2019-01-14)"
                sodipodi:docname="triple_union_cano_sats.svg"
            >
                <defs id="defs5938">
                <linearGradient
                    gradientTransform="matrix(0.88367614,0,0,0.88504699,-5.907311,40.531135)"
                    inkscape:collect="always"
                    xlink:href="#linearGradient4736"
                    id="linearGradient4730"
                    gradientUnits="userSpaceOnUse"
                    x1="59.719215"
                    y1="14.773782"
                    x2="60.935883"
                    y2="127.15653"
                    spreadMethod="reflect"
                />
                <linearGradient id="linearGradient4736" inkscape:collect="always">
                    <stop
                    id="stop4732"
                    offset="0"
                    style="stop-color:#0669b2;stop-opacity:1;"
                    />
                    <stop
                    style="stop-color:#00779f;stop-opacity:1"
                    offset="0.5"
                    id="stop4738"
                    />
                    <stop
                    id="stop4734"
                    offset="1"
                    style="stop-color:#00779f;stop-opacity:1"
                    />
                </linearGradient>
                </defs>
                <sodipodi:namedview
                id="base"
                pagecolor="#ffffff"
                bordercolor="#666666"
                borderopacity="1.0"
                inkscape:pageopacity="0.0"
                inkscape:pageshadow="2"
                inkscape:zoom="0.7"
                inkscape:cx="-16.929602"
                inkscape:cy="335.62296"
                inkscape:document-units="mm"
                inkscape:current-layer="layer1"
                showgrid="false"
                inkscape:window-width="1366"
                inkscape:window-height="705"
                inkscape:window-x="-8"
                inkscape:window-y="79"
                inkscape:window-maximized="1"
                />
                <metadata id="metadata5941">
                <rdf:RDF>
                    <cc:Work rdf:about="">
                    <dc:format>image/svg+xml</dc:format>
                    <dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
                    <dc:title></dc:title>
                    </cc:Work>
                </rdf:RDF>
                </metadata>
                <g
                inkscape:label="Camada 1"
                inkscape:groupmode="layer"
                id="layer1"
                transform="translate(0,-227)"
                >
                <path
                    inkscape:connector-curvature="0"
                    id="path4658"
                    d="m 0,227.00003 v 38.72152 h 4.342306 v -2.34168 h 26.92102 v 29.27108 H 28.925269 V 297 H 67.58682 v -4.34905 h -2.25495 v -29.27108 h 30.32582 v 2.34168 H 100 v -38.72152 h -4.34231 v 2.25708 c -30.64071,-0.13609 -64.140422,-0.009 -91.315384,8.9e-4 V 227 Z"
                    style="opacity:1;vector-effect:none;fill:url(#linearGradient4730);fill-opacity:1;stroke:none;stroke-width:0.44218066;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                    inkscape:label="triple_union"
                />
                </g>
            </svg>
        </div>';
    $juncao = '
        <div style="left: '.$left.'px; top: '.$top.'px; width: '.$width.'px; height: '.$height.'px; transform: rotate('.$angle.'deg);"
            class="componenteJuncao"
            id="'.$id.'">
            '.$svg.'
        </div>
    ';
    echo $juncao;
}

function Caixa($id, $nome, $top, $left, $width, $height, $angle, $classe, $index){
    $titulo = strlen($nome) > 0 ? '
            <p class="text-center white-text ">
                <b>'.$nome.'</b>
            </p>' : '';
    $caixa = '
        <div style="left: '.$left.'px; top: '.$top.'px; width: '.$width.'px; height: '.$height.'px; transform: rotate('.$angle.'deg); z-index:'.$index.';"
            class="'.$classe.' box"
            id="'.$id.'">
            '.$titulo.'
        </div>
    ';
    echo $caixa;
}

function Distribuicao($id, $max, $res, $nome, $top, $left, $width, $height, $angle, $type){
    $distribuicao = '
        <div style="left: '.$left.'px; top: '.$top.'px; width: '.$width.'px; height: '.$height.'px; transform: rotate('.$angle.'deg);"
            class="distribuicao"
            id="'.$id.'">
            <svg
                xmlns:dc="http://purl.org/dc/elements/1.1/"
                xmlns:cc="http://creativecommons.org/ns#"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:svg="http://www.w3.org/2000/svg"
                xmlns="http://www.w3.org/2000/svg"
                xmlns:xlink="http://www.w3.org/1999/xlink"
                id="'.$id.'-svg-distribuicao"
                version="1.1"
                viewBox="0 0 162 94"
                height="'.$height.'"
                width="'.$width.'"
                >
                <defs id="defs2">
                    <linearGradient id="linearGradient7106">
                    <stop
                        id="stop7102"
                        offset="0"
                        style="stop-color:#925136;stop-opacity:1;"
                    />
                    <stop
                        id="stop7104"
                        offset="1"
                        style="stop-color:#925136;stop-opacity:0;"
                    />
                    </linearGradient>
                    <linearGradient
                    spreadMethod="reflect"
                    gradientTransform="matrix(3.7795276,0,0,4.5094363,0,-102.58653)"
                    gradientUnits="userSpaceOnUse"
                    y2="132.33858"
                    x2="36.22504"
                    y1="133.8353"
                    x1="34.798489"
                    id="linearGradient7108"
                    xlink:href="#linearGradient7106"
                    />
                    <linearGradient
                    spreadMethod="reflect"
                    y2="132.33858"
                    x2="36.22504"
                    y1="133.8353"
                    x1="34.798489"
                    gradientTransform="matrix(3.7795276,0,0,4.5094363,0,-102.58653)"
                    gradientUnits="userSpaceOnUse"
                    id="linearGradient7145"
                    xlink:href="#linearGradient7106"
                    />
                    <linearGradient
                    spreadMethod="reflect"
                    y2="132.33858"
                    x2="36.22504"
                    y1="133.8353"
                    x1="34.798489"
                    gradientTransform="matrix(3.7795276,0,0,4.5094363,0,-102.58653)"
                    gradientUnits="userSpaceOnUse"
                    id="linearGradient7187"
                    xlink:href="#linearGradient7106"
                    />
                    <linearGradient
                    spreadMethod="reflect"
                    y2="132.33858"
                    x2="36.22504"
                    y1="133.8353"
                    x1="34.798489"
                    gradientTransform="matrix(3.7795276,0,0,4.5094363,0,-102.58653)"
                    gradientUnits="userSpaceOnUse"
                    id="linearGradient7203"
                    xlink:href="#linearGradient7106"
                    />
                    <linearGradient
                    spreadMethod="reflect"
                    y2="132.33858"
                    x2="36.22504"
                    y1="133.8353"
                    x1="34.798489"
                    gradientTransform="matrix(3.7795276,0,0,4.5094363,0,-102.58653)"
                    gradientUnits="userSpaceOnUse"
                    id="linearGradient7219"
                    xlink:href="#linearGradient7106"
                    />
                    <linearGradient
                    spreadMethod="reflect"
                    y2="132.33858"
                    x2="36.22504"
                    y1="133.8353"
                    x1="34.798489"
                    gradientTransform="matrix(3.7795276,0,0,4.5094363,0,-102.58653)"
                    gradientUnits="userSpaceOnUse"
                    id="linearGradient7235"
                    xlink:href="#linearGradient7106"
                    />
                    <linearGradient
                    spreadMethod="reflect"
                    y2="132.33858"
                    x2="36.22504"
                    y1="133.8353"
                    x1="34.798489"
                    gradientTransform="matrix(3.7795276,0,0,4.5094363,0,-102.58653)"
                    gradientUnits="userSpaceOnUse"
                    id="linearGradient7251"
                    xlink:href="#linearGradient7106"
                    />
                    <linearGradient
                    spreadMethod="reflect"
                    y2="132.33858"
                    x2="36.22504"
                    y1="133.8353"
                    x1="34.798489"
                    gradientTransform="matrix(3.7795276,0,0,4.5094363,0,-102.58653)"
                    gradientUnits="userSpaceOnUse"
                    id="linearGradient7267"
                    xlink:href="#linearGradient7106"
                    />
                </defs>
                <metadata id="metadata5">
                    <rdf:RDF>
                    <cc:Work rdf:about="">
                        <dc:format>image/svg+xml</dc:format>
                        <dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
                        <dc:title></dc:title>
                    </cc:Work>
                    </rdf:RDF>
                </metadata>
                <g transform="translate(0,-203)" id="layer1">
                    <g
                    transform="translate(-15.491019,108.81887)"
                    style="display:inline"
                    id="g7531"
                    >
                    <path
                        id="path5463"
                        d="M 16.241268,140.15841 95.557856,94.895966 176.74077,143.27548 98.966813,187.62386 Z"
                        style="display:inline;opacity:1;vector-effect:none;fill:#a9c4cd;fill-opacity:1;stroke:none;stroke-width:0.44264889;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                    />
                    <path
                        transform="matrix(0.77258083,-0.63491642,0.76379457,0.64545941,0,0)"
                        style="display:inline;opacity:1;vector-effect:none;fill:#6db257;fill-opacity:1;stroke:none;stroke-width:0.39174184;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                        d="m -91.842707,127.01134 76.532764,13.73349 13.7850301,79.34773 -75.0212701,-13.48966 z"
                        id="rect5460"
                    />
                    <g
                        style="display:inline"
                        id="g7217"
                        transform="translate(26.566591,19.831399)"
                    >
                        <g id="g7209">
                        <path
                            d="m 136.47461,491.33789 c -0.98152,0 -1.77149,0.78997 -1.77149,1.77149 v 36.32031 c 0,0.32623 0.0989,0.62488 0.25196,0.88672 0.0244,-0.0205 0.0482,-0.0419 0.0723,-0.0625 -0.0113,0.004 -0.0422,0.0236 -0.0332,0.0156 0.0867,-0.0733 0.16743,-0.15424 0.26563,-0.21094 0.10801,-0.0626 0.24118,-0.0929 0.37695,-0.10547 0.13577,-0.0124 0.27438,-0.008 0.39453,0.006 0.1245,0.034 0.25531,-0.003 0.38086,0.0117 0.15224,0.0189 0.29311,0.0603 0.43945,0.0996 0.13285,0.008 0.25211,0.0767 0.38086,0.10352 0.0136,0.003 0.043,-0.0155 0.0625,-0.0195 0.0333,-0.004 0.0662,-0.006 0.0996,-0.01 0.18093,-0.0119 0.27869,0.0221 0.39258,0.084 0.0612,-7.7e-4 0.12243,-0.002 0.18359,-0.002 0.17809,0.002 0.35511,1.9e-4 0.53321,0 0.0234,1e-5 0.0469,0 0.0703,0 0.12122,-0.24015 0.19531,-0.50851 0.19531,-0.79687 v -36.32031 c 0,-0.98152 -0.78996,-1.77149 -1.77148,-1.77149 z"
                            style="display:inline;opacity:1;vector-effect:none;fill:#794327;fill-opacity:1;stroke:none;stroke-width:1.60390437;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            transform="scale(0.26458333)"
                            id="path7205"
                        />
                        <path
                            d="m 136.0293,491.33789 c -0.73451,0 -1.32618,0.78997 -1.32618,1.77149 v 36.32031 c 0,0.25334 0.0422,0.49339 0.11329,0.71093 h 0.31836 c 0.0663,0 0.13341,-0.008 0.19921,0 0.0967,0.0113 0.18966,0.0377 0.28516,0.0566 0.0254,0.006 0.051,0.0125 0.0762,0.0195 0.0276,-0.005 0.055,-0.005 0.0859,-0.01 0.25123,-0.0378 0.46664,-0.0199 0.66797,0.15625 -0.0254,-0.0113 -0.10097,-0.0425 -0.0762,-0.0293 0.0527,0.0265 0.10815,0.0451 0.16015,0.0723 0.0242,0.0114 0.0624,0.0197 0.0664,0.0469 0.003,0.0185 -0.0769,0.0215 -0.0566,0.0215 0.0575,0 0.11487,-0.0135 0.17187,-0.0215 0.0726,0.004 0.1441,0.008 0.2168,0.0117 0.0485,0.014 0.0975,0.0263 0.14648,0.0391 0.17113,-0.29838 0.27735,-0.66758 0.27735,-1.07422 v -36.32031 c 0,-0.98152 -0.59167,-1.77149 -1.32617,-1.77149 z"
                            style="display:inline;opacity:1;vector-effect:none;fill:url(#linearGradient7219);fill-opacity:1;stroke:none;stroke-width:1.29526961;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            transform="scale(0.26458333)"
                            id="path7207"
                        />
                        </g>
                        <g id="g7215">
                        <path
                            id="path7211"
                            d="m 33.023307,129.79987 c 0.165809,0.98597 0.785387,0.3263 1.422622,0.64492 0.637235,0.31862 0.705217,0.28291 1.157066,0.1323 0.451849,-0.15062 0.276133,-0.33074 0.843359,-0.33074 0.567226,0 0.726157,0.39132 1.352951,0.18239 0.626794,-0.20893 0.45413,-0.13819 1.16059,-0.57926 0.70646,-0.44107 1.052781,-0.96643 1.372527,-1.71979 0.319746,-0.75336 0.693698,-0.95426 0.464825,-2.0083 -0.228873,-1.05405 -0.11062,-1.37489 -0.712872,-1.87777 -0.602252,-0.50288 -0.446893,-0.98285 -0.856917,-1.39287 -0.410023,-0.41002 -0.799047,-0.32699 -1.308671,-0.70256 -0.509623,-0.37556 -1.391401,-0.55473 -2.157278,-0.48945 -0.765877,0.0653 -1.482282,0.43279 -2.035164,0.96881 -0.552882,0.53602 -0.680309,0.10105 -1.452857,0.87359 -0.772548,0.77253 -0.323878,0.99008 -0.523492,1.85042 -0.199614,0.86034 -0.714776,1.09126 -0.33073,1.78594 0.384046,0.69468 -0.03042,0.84563 0.479558,1.48828 0.509976,0.64264 0.958674,0.18811 1.124483,1.17409 z"
                            style="fill:#5d973c;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                        />
                        <path
                            style="fill:#487b2e;fill-opacity:1;stroke:none;stroke-width:0.24757226px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                            d="m 32.901261,129.32272 c 0.154196,0.92827 0.730381,0.30721 1.322986,0.60718 0.592605,0.29998 0.655826,0.26636 1.076029,0.12456 0.420203,-0.1418 0.256793,-0.31139 0.784293,-0.31139 0.527499,0 0.675299,0.36842 1.258194,0.17172 0.582895,-0.1967 0.422324,-0.1301 1.079306,-0.54536 0.656981,-0.41526 0.979048,-0.90989 1.276399,-1.61917 0.297352,-0.70927 0.645114,-0.89841 0.43227,-1.89078 -0.212843,-0.99237 -0.102876,-1.29443 -0.662944,-1.76789 -0.560073,-0.47345 -0.415595,-0.92533 -0.796901,-1.31136 -0.381307,-0.38603 -0.743084,-0.30786 -1.217016,-0.66145 -0.47393,-0.35359 -1.293951,-0.52227 -2.006189,-0.46081 -0.712238,0.0615 -1.378467,0.40747 -1.892628,0.91212 -0.51416,0.50465 -0.632661,0.0951 -1.351102,0.82246 -0.718441,0.72734 -0.301195,0.93216 -0.486829,1.74215 -0.185634,0.81 -0.664715,1.0274 -0.307567,1.68143 0.357149,0.65404 -0.02829,0.79616 0.445972,1.4012 0.474259,0.60504 0.891531,0.17711 1.045727,1.10539 z"
                            id="path7213"
                        />
                        </g>
                    </g>
                    <g id="g7249" transform="translate(77.45366,-2.3674325)">
                        <g
                        id="g7241"
                        transform="matrix(0.50816809,0,0,0.75105971,26.326383,30.60931)"
                        style="display:inline"
                        >
                        <path
                            id="path7237"
                            transform="scale(0.26458333)"
                            style="display:inline;opacity:1;vector-effect:none;fill:#794327;fill-opacity:1;stroke:none;stroke-width:1.60390437;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 136.47461,491.33789 c -0.98152,0 -1.77149,0.78997 -1.77149,1.77149 v 36.32031 c 0,0.32623 0.0989,0.62488 0.25196,0.88672 0.0244,-0.0205 0.0482,-0.0419 0.0723,-0.0625 -0.0113,0.004 -0.0422,0.0236 -0.0332,0.0156 0.0867,-0.0733 0.16743,-0.15424 0.26563,-0.21094 0.10801,-0.0626 0.24118,-0.0929 0.37695,-0.10547 0.13577,-0.0124 0.27438,-0.008 0.39453,0.006 0.1245,0.034 0.25531,-0.003 0.38086,0.0117 0.15224,0.0189 0.29311,0.0603 0.43945,0.0996 0.13285,0.008 0.25211,0.0767 0.38086,0.10352 0.0136,0.003 0.043,-0.0155 0.0625,-0.0195 0.0333,-0.004 0.0662,-0.006 0.0996,-0.01 0.18093,-0.0119 0.27869,0.0221 0.39258,0.084 0.0612,-7.7e-4 0.12243,-0.002 0.18359,-0.002 0.17809,0.002 0.35511,1.9e-4 0.53321,0 0.0234,1e-5 0.0469,0 0.0703,0 0.12122,-0.24015 0.19531,-0.50851 0.19531,-0.79687 v -36.32031 c 0,-0.98152 -0.78996,-1.77149 -1.77148,-1.77149 z"
                        />
                        <path
                            id="path7239"
                            transform="scale(0.26458333)"
                            style="display:inline;opacity:1;vector-effect:none;fill:url(#linearGradient7251);fill-opacity:1;stroke:none;stroke-width:1.29526961;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 136.0293,491.33789 c -0.73451,0 -1.32618,0.78997 -1.32618,1.77149 v 36.32031 c 0,0.25334 0.0422,0.49339 0.11329,0.71093 h 0.31836 c 0.0663,0 0.13341,-0.008 0.19921,0 0.0967,0.0113 0.18966,0.0377 0.28516,0.0566 0.0254,0.006 0.051,0.0125 0.0762,0.0195 0.0276,-0.005 0.055,-0.005 0.0859,-0.01 0.25123,-0.0378 0.46664,-0.0199 0.66797,0.15625 -0.0254,-0.0113 -0.10097,-0.0425 -0.0762,-0.0293 0.0527,0.0265 0.10815,0.0451 0.16015,0.0723 0.0242,0.0114 0.0624,0.0197 0.0664,0.0469 0.003,0.0185 -0.0769,0.0215 -0.0566,0.0215 0.0575,0 0.11487,-0.0135 0.17187,-0.0215 0.0726,0.004 0.1441,0.008 0.2168,0.0117 0.0485,0.014 0.0975,0.0263 0.14648,0.0391 0.17113,-0.29838 0.27735,-0.66758 0.27735,-1.07422 v -36.32031 c 0,-0.98152 -0.59167,-1.77149 -1.32617,-1.77149 z"
                        />
                        </g>
                        <g id="g7247">
                        <path
                            id="path7243"
                            d="m 44.946093,128.26198 c 0.105839,0.10626 0.584229,0.48519 0.727604,0.19844 0.02863,-0.0573 0.01159,-0.14387 0.06615,-0.19844 0.05761,-0.0576 0.181483,0.17342 0.231511,0.19844 0.02201,0.011 0.325225,0.011 0.330729,0 0.03371,-0.0674 -0.02548,-0.21877 0.06615,-0.26459 0.13984,-0.0699 0.456475,0.23806 0.595312,0.0992 0.0451,-0.0451 0.05726,-0.29765 0.165365,-0.29765 0.0644,0 0.153656,0.37551 0.23151,0.29765 0.165177,-0.16518 0.04479,-0.35416 0.132292,-0.52916 0.03399,-0.068 0.304612,0.0923 0.330729,0.0661 0.04545,-0.0454 0.0833,-0.51201 0.03307,-0.56224 -0.01558,-0.0156 -0.06615,0.022 -0.06615,0 0,-0.028 0.281985,-0.0505 0.396875,-0.16536 0.170773,-0.17078 -0.246647,-0.43275 -0.23151,-0.46302 0.06965,-0.13931 0.410096,-0.40351 0.297656,-0.62839 -0.04614,-0.0923 -0.252466,-0.0419 -0.297656,-0.13229 -0.0456,-0.0912 0.135223,-0.40567 0.165364,-0.49609 0.06959,-0.20878 -0.199422,-0.10217 -0.23151,-0.19844 -0.04296,-0.12889 0.0105,-0.29918 -0.03307,-0.42995 -0.02453,-0.0736 -0.123415,-0.12785 -0.198438,-0.16536 -0.0093,-0.005 -0.132291,0.004 -0.132291,0 0,-0.0397 0.04837,-0.0637 0.06614,-0.0992 0.03135,-0.0627 0.0317,-0.51469 0,-0.56224 -0.03276,-0.0491 -0.07695,-0.11385 -0.132291,-0.13229 -0.03302,-0.011 -0.187235,0.0224 -0.198438,0 -0.05944,-0.11888 0.01614,-0.33154 -0.03307,-0.42995 -0.0586,-0.1172 -0.266829,-0.007 -0.297656,-0.0992 -0.04371,-0.13112 0.06132,-0.34039 0,-0.46302 -0.02999,-0.06 -0.198437,0.034 -0.198437,-0.0331 0,-0.11173 0.17916,-0.4685 0.09922,-0.62838 -0.04942,-0.0988 -0.363802,-0.0425 -0.363802,-0.0661 0,-0.1322 0.02951,-0.50679 -0.03307,-0.69453 -0.03472,-0.10416 -0.274746,0.0229 -0.330729,-0.0331 -0.0096,-0.01 0.05422,-0.43727 -0.03307,-0.61185 -0.06751,-0.13503 -0.215849,-0.16627 -0.264583,-0.21497 -0.07468,-0.0747 -0.0071,-0.28038 -0.132292,-0.36381 -0.08872,-0.0592 -0.330729,-0.32932 -0.330729,-0.16536 0,0.0136 -0.0088,0.0905 0,0.0992 0.01103,0.011 0.02204,-0.022 0.03307,-0.0331 0.200316,-0.20032 0.238109,-0.31753 0.09922,-0.59532 -0.04997,-0.0999 -0.206341,-0.0654 -0.256315,-0.16532 -0.06174,-0.12348 0.02039,-0.24033 -0.04134,-0.3638 -0.03095,-0.0619 -0.182777,-0.18974 -0.297657,-0.1323 -0.07075,0.0354 -0.09922,0.13824 -0.09922,0.1323 0,-0.0206 0.01685,-0.18159 0,-0.19844 0,0 0.0018,-0.19983 -0.177667,-0.11008 -0.07234,0.0362 -0.139078,0.12534 -0.124127,0.2093 0.02224,0.0834 -0.02386,0.27019 -0.06615,0.18601 0,0 -0.05488,-0.0171 -0.06201,-0.0207 -0.105585,-0.0528 -0.25798,-0.0177 -0.305925,0.0785 -0.117639,0.23528 0.164973,0.31833 0.107487,0.31833 -0.288925,0 -0.311104,-0.0724 -0.44235,0.19017 -0.07048,0.14096 0.107507,0.30592 0.07855,0.30592 -0.658056,0 -0.205991,0.47058 -0.23151,0.49609 -0.10727,0.10727 -0.564973,0.058 -0.463021,0.36381 0.01188,0.0356 0.07119,0.1136 0.09922,0.13229 0.01834,0.0122 0.06614,-0.022 0.06614,0 0,0.0562 -0.115085,0.008 -0.165364,0.0331 -0.08407,0.042 -0.327554,0.19209 -0.363802,0.26458 -0.138208,0.27642 0.264583,0.15169 0.264583,0.33073 0,0.002 -0.193765,-0.002 -0.198437,0 -0.03285,0.0164 -0.191543,0.11851 -0.198438,0.1323 -0.08115,0.1623 0.09922,0.41572 0.09922,0.46302 0,0.0441 -0.08819,0 -0.132292,0 -0.1,0 -0.186081,0.0538 -0.23151,0.0992 -0.01154,0.0115 0.01373,0.0924 0,0.0992 -0.543674,0.27184 0.422574,0.33073 -0.165365,0.33073 -0.257926,0 -0.297656,0.41245 -0.297656,0.62839 0,0.0573 0.132805,0.032 0.09922,0.0992 -0.0578,0.11559 -0.0046,-0.0616 -0.09922,0.0331 -0.01558,0.0156 0.0099,0.0464 0,0.0661 -0.06361,0.12722 -0.233836,0.17233 -0.297656,0.3638 -0.0084,0.0253 -0.02098,0.24359 0,0.26458 0.0075,0.007 0.150582,0.0148 0.09922,0.0661 -0.107569,0.10757 -0.262985,-0.005 -0.330729,0.19843 -0.0534,0.16022 0.256453,0.37194 0.165365,0.46303 -0.0754,0.0754 -0.42972,0.13274 -0.330729,0.33072 0.0081,0.0161 0.145285,0.0791 0.165364,0.0992 0.0865,0.0865 0.0587,0.1323 0.198438,0.1323 0.06615,0 -0.135686,-0.0209 -0.198438,0 -0.09233,0.0308 -0.265856,0.20225 -0.297656,0.29765 -0.01924,0.0577 -0.147865,0.44745 -0.06615,0.52917 0.01818,0.0182 0.306184,0.0907 0.264584,0.13229 -0.01656,0.0166 -0.110609,0.0114 -0.132292,0.0331 -0.146923,0.14692 -0.198438,0.31828 -0.198438,0.59532 0,0.17895 0.282859,0.11749 0.363802,0.19843 0.02455,0.0246 -0.0898,0.10403 -0.09922,0.13229 -0.02432,0.073 -0.07516,0.27964 -0.03307,0.36381 0.0965,0.193 0.501565,0.13411 0.694531,0.19843 0.079,0.0263 0.01249,0.23592 0.03307,0.29766 0.05531,0.16593 0.210219,0.27637 0.297656,0.3638 0.05226,0.0523 0.580541,0.0148 0.628386,-0.0331 0.05937,-0.0594 0.01527,-0.23151 0.09922,-0.23151 0.0065,0 -0.0086,0.21423 0,0.23151 0.05139,0.10277 0.07826,0.16536 0.198438,0.16536 0.08658,0 0.472186,0.057 0.529167,0 0.140168,-0.14016 0.18167,-0.42202 0.330729,-0.19843 0.106537,0.1598 0.47073,0.25301 0.595312,0.0661 0.03845,-0.0577 0.03463,-0.16692 0.06615,-0.19844 0.0059,-0.006 0.124836,0.1451 0.165365,0.16537 0.05867,0.0293 0.280095,-0.0604 0.264583,-0.0992 -0.02048,-0.0512 -0.06615,-0.0882 -0.09922,-0.13229 z"
                            style="fill:#5a963a;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                        />
                        <path
                            style="fill:#497c2f;fill-opacity:1;stroke:none;stroke-width:0.23497705px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                            d="m 44.299271,127.67574 c 0.08887,0.0998 0.490573,0.45574 0.610964,0.1864 0.02404,-0.0538 0.0097,-0.13514 0.05555,-0.1864 0.04838,-0.0541 0.152391,0.1629 0.194399,0.1864 0.01848,0.0103 0.273089,0.0103 0.27771,0 0.0283,-0.0633 -0.02139,-0.20549 0.05555,-0.24853 0.117422,-0.0657 0.383298,0.22361 0.499879,0.0932 0.03787,-0.0424 0.04808,-0.27959 0.138855,-0.27959 0.05407,0 0.129025,0.35272 0.194398,0.27959 0.138699,-0.15516 0.03761,-0.33267 0.111085,-0.49704 0.02854,-0.0639 0.255781,0.0867 0.277711,0.0621 0.03816,-0.0426 0.06995,-0.48093 0.02777,-0.52811 -0.01309,-0.0147 -0.05555,0.0207 -0.05555,0 0,-0.0263 0.23678,-0.0474 0.333253,-0.15532 0.143397,-0.16042 -0.207108,-0.40649 -0.194398,-0.43492 0.05848,-0.13085 0.344356,-0.37902 0.24994,-0.59025 -0.03874,-0.0867 -0.211993,-0.0393 -0.24994,-0.12426 -0.03829,-0.0857 0.113546,-0.38104 0.138855,-0.46597 0.05844,-0.19611 -0.167452,-0.096 -0.194397,-0.1864 -0.03608,-0.12107 0.0088,-0.28102 -0.02777,-0.40385 -0.0206,-0.0691 -0.103631,-0.12009 -0.166628,-0.15533 -0.0078,-0.005 -0.111083,0.004 -0.111083,0 0,-0.0373 0.04062,-0.0598 0.05554,-0.0932 0.02632,-0.0589 0.02661,-0.48345 0,-0.52812 -0.02751,-0.0461 -0.06462,-0.10694 -0.111084,-0.12426 -0.02773,-0.0103 -0.15722,0.021 -0.166626,0 -0.04991,-0.11166 0.01355,-0.31141 -0.02777,-0.40385 -0.04921,-0.11009 -0.224054,-0.007 -0.24994,-0.0932 -0.0367,-0.12316 0.05149,-0.31973 0,-0.43492 -0.02519,-0.0563 -0.166625,0.0319 -0.166625,-0.0311 0,-0.10495 0.150439,-0.44006 0.08331,-0.59024 -0.04149,-0.0928 -0.305483,-0.0399 -0.305483,-0.0621 0,-0.12418 0.02478,-0.47603 -0.02777,-0.65238 -0.02916,-0.0978 -0.230702,0.0215 -0.27771,-0.0311 -0.0081,-0.009 0.04553,-0.41073 -0.02777,-0.57471 -0.05669,-0.12683 -0.181247,-0.15618 -0.222168,-0.20192 -0.06271,-0.0702 -0.006,-0.26336 -0.111085,-0.34173 -0.0745,-0.0556 -0.277711,-0.30933 -0.277711,-0.15532 0,0.0128 -0.0074,0.085 0,0.0932 0.0093,0.0103 0.0185,-0.0207 0.02777,-0.0311 0.168203,-0.18817 0.199938,-0.29826 0.08332,-0.55919 -0.04196,-0.0938 -0.173264,-0.0614 -0.215227,-0.15529 -0.05185,-0.11598 0.01712,-0.22574 -0.03471,-0.34171 -0.02598,-0.0581 -0.153477,-0.17823 -0.249941,-0.12427 -0.0594,0.0333 -0.08331,0.12985 -0.08331,0.12427 0,-0.0194 0.01415,-0.17057 0,-0.1864 0,0 0.0015,-0.1877 -0.149186,-0.1034 -0.06075,0.034 -0.116783,0.11774 -0.104229,0.1966 0.01867,0.0783 -0.02003,0.25379 -0.05555,0.17472 0,0 -0.04608,-0.0161 -0.05207,-0.0194 -0.08866,-0.0496 -0.216624,-0.0166 -0.256883,0.0737 -0.09878,0.221 0.138526,0.29901 0.09025,0.29901 -0.242609,0 -0.261232,-0.068 -0.371439,0.17863 -0.05918,0.1324 0.09027,0.28735 0.06596,0.28735 -0.552566,0 -0.17297,0.44201 -0.194398,0.46598 -0.09007,0.10075 -0.474404,0.0545 -0.388795,0.34172 0.01,0.0334 0.05978,0.10671 0.08331,0.12426 0.0154,0.0115 0.05554,-0.0207 0.05554,0 0,0.0528 -0.09664,0.008 -0.138855,0.0311 -0.07059,0.0395 -0.275045,0.18043 -0.305482,0.24852 -0.116052,0.25965 0.222169,0.14249 0.222169,0.31066 0,0.002 -0.162703,-0.002 -0.166627,0 -0.02758,0.0154 -0.160837,0.11132 -0.166627,0.12427 -0.06815,0.15245 0.08331,0.39049 0.08331,0.43492 0,0.0414 -0.07405,0 -0.111085,0 -0.08397,0 -0.156251,0.0505 -0.194397,0.0932 -0.0097,0.0108 0.01153,0.0868 0,0.0932 -0.456519,0.25534 0.354832,0.31066 -0.138856,0.31066 -0.216579,0 -0.24994,0.38741 -0.24994,0.59025 0,0.0538 0.111515,0.0301 0.08331,0.0932 -0.04853,0.10857 -0.0038,-0.0579 -0.08331,0.0311 -0.01309,0.0147 0.0084,0.0436 0,0.0621 -0.05342,0.1195 -0.196351,0.16187 -0.24994,0.34172 -0.007,0.0238 -0.01762,0.22881 0,0.24852 0.0063,0.007 0.126443,0.0139 0.08331,0.0621 -0.09033,0.10104 -0.220826,-0.005 -0.27771,0.18639 -0.04483,0.15049 0.215342,0.34936 0.138855,0.43492 -0.06331,0.0708 -0.360833,0.12468 -0.27771,0.31065 0.0068,0.0151 0.121995,0.0743 0.138855,0.0932 0.07263,0.0812 0.04929,0.12427 0.166627,0.12427 0.05555,0 -0.113935,-0.0196 -0.166627,0 -0.07753,0.0289 -0.223238,0.18997 -0.24994,0.27958 -0.01616,0.0542 -0.124161,0.42029 -0.05555,0.49705 0.01526,0.0171 0.2571,0.0852 0.222169,0.12426 -0.0139,0.0156 -0.09288,0.0107 -0.111085,0.0311 -0.12337,0.138 -0.166627,0.29896 -0.166627,0.55919 0,0.16808 0.237515,0.11036 0.305483,0.18638 0.02061,0.0231 -0.0754,0.0977 -0.08332,0.12426 -0.02042,0.0686 -0.06311,0.26267 -0.02777,0.34173 0.08103,0.18129 0.42116,0.12597 0.583193,0.18639 0.06633,0.0247 0.01049,0.2216 0.02777,0.27959 0.04644,0.15586 0.176519,0.25959 0.24994,0.34172 0.04389,0.0491 0.487475,0.0139 0.527651,-0.0311 0.04986,-0.0558 0.01282,-0.21746 0.08331,-0.21746 0.0054,0 -0.0072,0.20122 0,0.21746 0.04315,0.0965 0.06571,0.15532 0.166627,0.15532 0.0727,0 0.396492,0.0535 0.444338,0 0.117698,-0.13165 0.152548,-0.39641 0.277711,-0.18639 0.08946,0.1501 0.395268,0.23766 0.499879,0.0621 0.03228,-0.0542 0.02908,-0.15679 0.05555,-0.18639 0.005,-0.006 0.104824,0.13629 0.138856,0.15533 0.04927,0.0275 0.235194,-0.0567 0.222168,-0.0932 -0.0172,-0.0481 -0.05555,-0.0829 -0.08331,-0.12426 z"
                            id="path7245"
                        />
                        </g>
                    </g>
                    <g id="g7265" transform="translate(68.019935,25.00688)">
                        <g
                        id="g7257"
                        transform="matrix(0.50816809,0,0,0.75105971,26.326383,30.60931)"
                        style="display:inline"
                        >
                        <path
                            id="path7253"
                            transform="scale(0.26458333)"
                            style="display:inline;opacity:1;vector-effect:none;fill:#794327;fill-opacity:1;stroke:none;stroke-width:1.60390437;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 136.47461,491.33789 c -0.98152,0 -1.77149,0.78997 -1.77149,1.77149 v 36.32031 c 0,0.32623 0.0989,0.62488 0.25196,0.88672 0.0244,-0.0205 0.0482,-0.0419 0.0723,-0.0625 -0.0113,0.004 -0.0422,0.0236 -0.0332,0.0156 0.0867,-0.0733 0.16743,-0.15424 0.26563,-0.21094 0.10801,-0.0626 0.24118,-0.0929 0.37695,-0.10547 0.13577,-0.0124 0.27438,-0.008 0.39453,0.006 0.1245,0.034 0.25531,-0.003 0.38086,0.0117 0.15224,0.0189 0.29311,0.0603 0.43945,0.0996 0.13285,0.008 0.25211,0.0767 0.38086,0.10352 0.0136,0.003 0.043,-0.0155 0.0625,-0.0195 0.0333,-0.004 0.0662,-0.006 0.0996,-0.01 0.18093,-0.0119 0.27869,0.0221 0.39258,0.084 0.0612,-7.7e-4 0.12243,-0.002 0.18359,-0.002 0.17809,0.002 0.35511,1.9e-4 0.53321,0 0.0234,1e-5 0.0469,0 0.0703,0 0.12122,-0.24015 0.19531,-0.50851 0.19531,-0.79687 v -36.32031 c 0,-0.98152 -0.78996,-1.77149 -1.77148,-1.77149 z"
                        />
                        <path
                            id="path7255"
                            transform="scale(0.26458333)"
                            style="display:inline;opacity:1;vector-effect:none;fill:url(#linearGradient7267);fill-opacity:1;stroke:none;stroke-width:1.29526961;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 136.0293,491.33789 c -0.73451,0 -1.32618,0.78997 -1.32618,1.77149 v 36.32031 c 0,0.25334 0.0422,0.49339 0.11329,0.71093 h 0.31836 c 0.0663,0 0.13341,-0.008 0.19921,0 0.0967,0.0113 0.18966,0.0377 0.28516,0.0566 0.0254,0.006 0.051,0.0125 0.0762,0.0195 0.0276,-0.005 0.055,-0.005 0.0859,-0.01 0.25123,-0.0378 0.46664,-0.0199 0.66797,0.15625 -0.0254,-0.0113 -0.10097,-0.0425 -0.0762,-0.0293 0.0527,0.0265 0.10815,0.0451 0.16015,0.0723 0.0242,0.0114 0.0624,0.0197 0.0664,0.0469 0.003,0.0185 -0.0769,0.0215 -0.0566,0.0215 0.0575,0 0.11487,-0.0135 0.17187,-0.0215 0.0726,0.004 0.1441,0.008 0.2168,0.0117 0.0485,0.014 0.0975,0.0263 0.14648,0.0391 0.17113,-0.29838 0.27735,-0.66758 0.27735,-1.07422 v -36.32031 c 0,-0.98152 -0.59167,-1.77149 -1.32617,-1.77149 z"
                        />
                        </g>
                        <g id="g7263">
                        <path
                            id="path7259"
                            d="m 44.946093,128.26198 c 0.105839,0.10626 0.584229,0.48519 0.727604,0.19844 0.02863,-0.0573 0.01159,-0.14387 0.06615,-0.19844 0.05761,-0.0576 0.181483,0.17342 0.231511,0.19844 0.02201,0.011 0.325225,0.011 0.330729,0 0.03371,-0.0674 -0.02548,-0.21877 0.06615,-0.26459 0.13984,-0.0699 0.456475,0.23806 0.595312,0.0992 0.0451,-0.0451 0.05726,-0.29765 0.165365,-0.29765 0.0644,0 0.153656,0.37551 0.23151,0.29765 0.165177,-0.16518 0.04479,-0.35416 0.132292,-0.52916 0.03399,-0.068 0.304612,0.0923 0.330729,0.0661 0.04545,-0.0454 0.0833,-0.51201 0.03307,-0.56224 -0.01558,-0.0156 -0.06615,0.022 -0.06615,0 0,-0.028 0.281985,-0.0505 0.396875,-0.16536 0.170773,-0.17078 -0.246647,-0.43275 -0.23151,-0.46302 0.06965,-0.13931 0.410096,-0.40351 0.297656,-0.62839 -0.04614,-0.0923 -0.252466,-0.0419 -0.297656,-0.13229 -0.0456,-0.0912 0.135223,-0.40567 0.165364,-0.49609 0.06959,-0.20878 -0.199422,-0.10217 -0.23151,-0.19844 -0.04296,-0.12889 0.0105,-0.29918 -0.03307,-0.42995 -0.02453,-0.0736 -0.123415,-0.12785 -0.198438,-0.16536 -0.0093,-0.005 -0.132291,0.004 -0.132291,0 0,-0.0397 0.04837,-0.0637 0.06614,-0.0992 0.03135,-0.0627 0.0317,-0.51469 0,-0.56224 -0.03276,-0.0491 -0.07695,-0.11385 -0.132291,-0.13229 -0.03302,-0.011 -0.187235,0.0224 -0.198438,0 -0.05944,-0.11888 0.01614,-0.33154 -0.03307,-0.42995 -0.0586,-0.1172 -0.266829,-0.007 -0.297656,-0.0992 -0.04371,-0.13112 0.06132,-0.34039 0,-0.46302 -0.02999,-0.06 -0.198437,0.034 -0.198437,-0.0331 0,-0.11173 0.17916,-0.4685 0.09922,-0.62838 -0.04942,-0.0988 -0.363802,-0.0425 -0.363802,-0.0661 0,-0.1322 0.02951,-0.50679 -0.03307,-0.69453 -0.03472,-0.10416 -0.274746,0.0229 -0.330729,-0.0331 -0.0096,-0.01 0.05422,-0.43727 -0.03307,-0.61185 -0.06751,-0.13503 -0.215849,-0.16627 -0.264583,-0.21497 -0.07468,-0.0747 -0.0071,-0.28038 -0.132292,-0.36381 -0.08872,-0.0592 -0.330729,-0.32932 -0.330729,-0.16536 0,0.0136 -0.0088,0.0905 0,0.0992 0.01103,0.011 0.02204,-0.022 0.03307,-0.0331 0.200316,-0.20032 0.238109,-0.31753 0.09922,-0.59532 -0.04997,-0.0999 -0.206341,-0.0654 -0.256315,-0.16532 -0.06174,-0.12348 0.02039,-0.24033 -0.04134,-0.3638 -0.03095,-0.0619 -0.182777,-0.18974 -0.297657,-0.1323 -0.07075,0.0354 -0.09922,0.13824 -0.09922,0.1323 0,-0.0206 0.01685,-0.18159 0,-0.19844 0,0 0.0018,-0.19983 -0.177667,-0.11008 -0.07234,0.0362 -0.139078,0.12534 -0.124127,0.2093 0.02224,0.0834 -0.02386,0.27019 -0.06615,0.18601 0,0 -0.05488,-0.0171 -0.06201,-0.0207 -0.105585,-0.0528 -0.25798,-0.0177 -0.305925,0.0785 -0.117639,0.23528 0.164973,0.31833 0.107487,0.31833 -0.288925,0 -0.311104,-0.0724 -0.44235,0.19017 -0.07048,0.14096 0.107507,0.30592 0.07855,0.30592 -0.658056,0 -0.205991,0.47058 -0.23151,0.49609 -0.10727,0.10727 -0.564973,0.058 -0.463021,0.36381 0.01188,0.0356 0.07119,0.1136 0.09922,0.13229 0.01834,0.0122 0.06614,-0.022 0.06614,0 0,0.0562 -0.115085,0.008 -0.165364,0.0331 -0.08407,0.042 -0.327554,0.19209 -0.363802,0.26458 -0.138208,0.27642 0.264583,0.15169 0.264583,0.33073 0,0.002 -0.193765,-0.002 -0.198437,0 -0.03285,0.0164 -0.191543,0.11851 -0.198438,0.1323 -0.08115,0.1623 0.09922,0.41572 0.09922,0.46302 0,0.0441 -0.08819,0 -0.132292,0 -0.1,0 -0.186081,0.0538 -0.23151,0.0992 -0.01154,0.0115 0.01373,0.0924 0,0.0992 -0.543674,0.27184 0.422574,0.33073 -0.165365,0.33073 -0.257926,0 -0.297656,0.41245 -0.297656,0.62839 0,0.0573 0.132805,0.032 0.09922,0.0992 -0.0578,0.11559 -0.0046,-0.0616 -0.09922,0.0331 -0.01558,0.0156 0.0099,0.0464 0,0.0661 -0.06361,0.12722 -0.233836,0.17233 -0.297656,0.3638 -0.0084,0.0253 -0.02098,0.24359 0,0.26458 0.0075,0.007 0.150582,0.0148 0.09922,0.0661 -0.107569,0.10757 -0.262985,-0.005 -0.330729,0.19843 -0.0534,0.16022 0.256453,0.37194 0.165365,0.46303 -0.0754,0.0754 -0.42972,0.13274 -0.330729,0.33072 0.0081,0.0161 0.145285,0.0791 0.165364,0.0992 0.0865,0.0865 0.0587,0.1323 0.198438,0.1323 0.06615,0 -0.135686,-0.0209 -0.198438,0 -0.09233,0.0308 -0.265856,0.20225 -0.297656,0.29765 -0.01924,0.0577 -0.147865,0.44745 -0.06615,0.52917 0.01818,0.0182 0.306184,0.0907 0.264584,0.13229 -0.01656,0.0166 -0.110609,0.0114 -0.132292,0.0331 -0.146923,0.14692 -0.198438,0.31828 -0.198438,0.59532 0,0.17895 0.282859,0.11749 0.363802,0.19843 0.02455,0.0246 -0.0898,0.10403 -0.09922,0.13229 -0.02432,0.073 -0.07516,0.27964 -0.03307,0.36381 0.0965,0.193 0.501565,0.13411 0.694531,0.19843 0.079,0.0263 0.01249,0.23592 0.03307,0.29766 0.05531,0.16593 0.210219,0.27637 0.297656,0.3638 0.05226,0.0523 0.580541,0.0148 0.628386,-0.0331 0.05937,-0.0594 0.01527,-0.23151 0.09922,-0.23151 0.0065,0 -0.0086,0.21423 0,0.23151 0.05139,0.10277 0.07826,0.16536 0.198438,0.16536 0.08658,0 0.472186,0.057 0.529167,0 0.140168,-0.14016 0.18167,-0.42202 0.330729,-0.19843 0.106537,0.1598 0.47073,0.25301 0.595312,0.0661 0.03845,-0.0577 0.03463,-0.16692 0.06615,-0.19844 0.0059,-0.006 0.124836,0.1451 0.165365,0.16537 0.05867,0.0293 0.280095,-0.0604 0.264583,-0.0992 -0.02048,-0.0512 -0.06615,-0.0882 -0.09922,-0.13229 z"
                            style="fill:#5a963a;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                        />
                        <path
                            style="fill:#497c2f;fill-opacity:1;stroke:none;stroke-width:0.23497705px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                            d="m 44.299271,127.67574 c 0.08887,0.0998 0.490573,0.45574 0.610964,0.1864 0.02404,-0.0538 0.0097,-0.13514 0.05555,-0.1864 0.04838,-0.0541 0.152391,0.1629 0.194399,0.1864 0.01848,0.0103 0.273089,0.0103 0.27771,0 0.0283,-0.0633 -0.02139,-0.20549 0.05555,-0.24853 0.117422,-0.0657 0.383298,0.22361 0.499879,0.0932 0.03787,-0.0424 0.04808,-0.27959 0.138855,-0.27959 0.05407,0 0.129025,0.35272 0.194398,0.27959 0.138699,-0.15516 0.03761,-0.33267 0.111085,-0.49704 0.02854,-0.0639 0.255781,0.0867 0.277711,0.0621 0.03816,-0.0426 0.06995,-0.48093 0.02777,-0.52811 -0.01309,-0.0147 -0.05555,0.0207 -0.05555,0 0,-0.0263 0.23678,-0.0474 0.333253,-0.15532 0.143397,-0.16042 -0.207108,-0.40649 -0.194398,-0.43492 0.05848,-0.13085 0.344356,-0.37902 0.24994,-0.59025 -0.03874,-0.0867 -0.211993,-0.0393 -0.24994,-0.12426 -0.03829,-0.0857 0.113546,-0.38104 0.138855,-0.46597 0.05844,-0.19611 -0.167452,-0.096 -0.194397,-0.1864 -0.03608,-0.12107 0.0088,-0.28102 -0.02777,-0.40385 -0.0206,-0.0691 -0.103631,-0.12009 -0.166628,-0.15533 -0.0078,-0.005 -0.111083,0.004 -0.111083,0 0,-0.0373 0.04062,-0.0598 0.05554,-0.0932 0.02632,-0.0589 0.02661,-0.48345 0,-0.52812 -0.02751,-0.0461 -0.06462,-0.10694 -0.111084,-0.12426 -0.02773,-0.0103 -0.15722,0.021 -0.166626,0 -0.04991,-0.11166 0.01355,-0.31141 -0.02777,-0.40385 -0.04921,-0.11009 -0.224054,-0.007 -0.24994,-0.0932 -0.0367,-0.12316 0.05149,-0.31973 0,-0.43492 -0.02519,-0.0563 -0.166625,0.0319 -0.166625,-0.0311 0,-0.10495 0.150439,-0.44006 0.08331,-0.59024 -0.04149,-0.0928 -0.305483,-0.0399 -0.305483,-0.0621 0,-0.12418 0.02478,-0.47603 -0.02777,-0.65238 -0.02916,-0.0978 -0.230702,0.0215 -0.27771,-0.0311 -0.0081,-0.009 0.04553,-0.41073 -0.02777,-0.57471 -0.05669,-0.12683 -0.181247,-0.15618 -0.222168,-0.20192 -0.06271,-0.0702 -0.006,-0.26336 -0.111085,-0.34173 -0.0745,-0.0556 -0.277711,-0.30933 -0.277711,-0.15532 0,0.0128 -0.0074,0.085 0,0.0932 0.0093,0.0103 0.0185,-0.0207 0.02777,-0.0311 0.168203,-0.18817 0.199938,-0.29826 0.08332,-0.55919 -0.04196,-0.0938 -0.173264,-0.0614 -0.215227,-0.15529 -0.05185,-0.11598 0.01712,-0.22574 -0.03471,-0.34171 -0.02598,-0.0581 -0.153477,-0.17823 -0.249941,-0.12427 -0.0594,0.0333 -0.08331,0.12985 -0.08331,0.12427 0,-0.0194 0.01415,-0.17057 0,-0.1864 0,0 0.0015,-0.1877 -0.149186,-0.1034 -0.06075,0.034 -0.116783,0.11774 -0.104229,0.1966 0.01867,0.0783 -0.02003,0.25379 -0.05555,0.17472 0,0 -0.04608,-0.0161 -0.05207,-0.0194 -0.08866,-0.0496 -0.216624,-0.0166 -0.256883,0.0737 -0.09878,0.221 0.138526,0.29901 0.09025,0.29901 -0.242609,0 -0.261232,-0.068 -0.371439,0.17863 -0.05918,0.1324 0.09027,0.28735 0.06596,0.28735 -0.552566,0 -0.17297,0.44201 -0.194398,0.46598 -0.09007,0.10075 -0.474404,0.0545 -0.388795,0.34172 0.01,0.0334 0.05978,0.10671 0.08331,0.12426 0.0154,0.0115 0.05554,-0.0207 0.05554,0 0,0.0528 -0.09664,0.008 -0.138855,0.0311 -0.07059,0.0395 -0.275045,0.18043 -0.305482,0.24852 -0.116052,0.25965 0.222169,0.14249 0.222169,0.31066 0,0.002 -0.162703,-0.002 -0.166627,0 -0.02758,0.0154 -0.160837,0.11132 -0.166627,0.12427 -0.06815,0.15245 0.08331,0.39049 0.08331,0.43492 0,0.0414 -0.07405,0 -0.111085,0 -0.08397,0 -0.156251,0.0505 -0.194397,0.0932 -0.0097,0.0108 0.01153,0.0868 0,0.0932 -0.456519,0.25534 0.354832,0.31066 -0.138856,0.31066 -0.216579,0 -0.24994,0.38741 -0.24994,0.59025 0,0.0538 0.111515,0.0301 0.08331,0.0932 -0.04853,0.10857 -0.0038,-0.0579 -0.08331,0.0311 -0.01309,0.0147 0.0084,0.0436 0,0.0621 -0.05342,0.1195 -0.196351,0.16187 -0.24994,0.34172 -0.007,0.0238 -0.01762,0.22881 0,0.24852 0.0063,0.007 0.126443,0.0139 0.08331,0.0621 -0.09033,0.10104 -0.220826,-0.005 -0.27771,0.18639 -0.04483,0.15049 0.215342,0.34936 0.138855,0.43492 -0.06331,0.0708 -0.360833,0.12468 -0.27771,0.31065 0.0068,0.0151 0.121995,0.0743 0.138855,0.0932 0.07263,0.0812 0.04929,0.12427 0.166627,0.12427 0.05555,0 -0.113935,-0.0196 -0.166627,0 -0.07753,0.0289 -0.223238,0.18997 -0.24994,0.27958 -0.01616,0.0542 -0.124161,0.42029 -0.05555,0.49705 0.01526,0.0171 0.2571,0.0852 0.222169,0.12426 -0.0139,0.0156 -0.09288,0.0107 -0.111085,0.0311 -0.12337,0.138 -0.166627,0.29896 -0.166627,0.55919 0,0.16808 0.237515,0.11036 0.305483,0.18638 0.02061,0.0231 -0.0754,0.0977 -0.08332,0.12426 -0.02042,0.0686 -0.06311,0.26267 -0.02777,0.34173 0.08103,0.18129 0.42116,0.12597 0.583193,0.18639 0.06633,0.0247 0.01049,0.2216 0.02777,0.27959 0.04644,0.15586 0.176519,0.25959 0.24994,0.34172 0.04389,0.0491 0.487475,0.0139 0.527651,-0.0311 0.04986,-0.0558 0.01282,-0.21746 0.08331,-0.21746 0.0054,0 -0.0072,0.20122 0,0.21746 0.04315,0.0965 0.06571,0.15532 0.166627,0.15532 0.0727,0 0.396492,0.0535 0.444338,0 0.117698,-0.13165 0.152548,-0.39641 0.277711,-0.18639 0.08946,0.1501 0.395268,0.23766 0.499879,0.0621 0.03228,-0.0542 0.02908,-0.15679 0.05555,-0.18639 0.005,-0.006 0.104824,0.13629 0.138856,0.15533 0.04927,0.0275 0.235194,-0.0567 0.222168,-0.0932 -0.0172,-0.0481 -0.05555,-0.0829 -0.08331,-0.12426 z"
                            id="path7261"
                        />
                        </g>
                    </g>
                    <g
                        id="g5541"
                        style="display:inline"
                        transform="translate(-0.51263021,0.08268229)"
                    >
                        <g style="display:inline" id="g5482">
                        <path
                            style="opacity:1;vector-effect:none;fill:#b84a4b;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 65.985009,149.66659 13.29092,7.83658 0.04178,13.03428 -13.416224,-7.75568 z"
                            id="rect5465"
                        />
                        <path
                            style="opacity:1;vector-effect:none;fill:#9e3738;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 79.275929,157.50317 10.688334,-7.0823 0.06493,13.98037 -10.711489,6.13621 z"
                            id="rect5468"
                        />
                        <path
                            style="fill:#b84a4b;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                            d="m 89.964263,150.42087 0.06493,13.98037 9.425789,5.50426 0.09449,-13.98511 z"
                            id="path5471"
                        />
                        <path
                            style="fill:#9c3b3c;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                            d="m 99.549473,155.92039 -0.09449,13.98511 10.866817,-6.2366 -0.0472,-13.84338 z"
                            id="path5473"
                        />
                        </g>
                        <g id="g6185">
                        <path
                            id="rect6165"
                            d="m 84.013474,160.7644 3.395405,-1.56236 -0.0065,6.70471 -3.263854,1.8691 z"
                            style="display:inline;opacity:1;vector-effect:none;fill:#7e4a3d;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                        />
                        <path
                            id="rect6168"
                            d="m 84.696069,161.3055 0.97707,-0.41709 -0.0248,2.62929 -0.96797,0.4505 z"
                            style="opacity:1;vector-effect:none;fill:#a07062;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                        />
                        <path
                            style="opacity:1;vector-effect:none;fill:#a07062;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 85.970611,160.74423 0.961121,-0.41224 -0.0089,2.62444 -0.979663,0.43881 z"
                            id="path6171"
                        />
                        <path
                            style="opacity:1;vector-effect:none;fill:#a07062;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 84.691935,164.29446 0.949428,-0.43563 0.0028,2.64783 -0.979663,0.56744 z"
                            id="path6173"
                        />
                        <path
                            id="path6175"
                            d="m 85.966477,163.73319 0.937735,-0.40799 -0.02055,2.5208 -0.937172,0.51653 z"
                            style="opacity:1;vector-effect:none;fill:#a07062;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                        />
                        <circle
                            r="0.12461104"
                            cy="163.08266"
                            cx="87.142395"
                            id="path6177"
                            style="opacity:1;vector-effect:none;fill:#a07062;fill-opacity:1;stroke:none;stroke-width:1.06568539;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                        />
                        </g>
                        <g id="g5679">
                        <g id="g5553">
                            <path
                            style="opacity:1;vector-effect:none;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.67708,157.0343 2.5576,-1.37861 0.0137,4.03209 -2.52452,1.53262 z"
                            id="rect5543"
                            />
                            <path
                            id="path5546"
                            d="m 100.84952,157.22255 2.21462,-1.19374 0.0116,3.49138 -2.18598,1.32709 z"
                            style="opacity:1;vector-effect:none;fill:#67beda;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                            <path
                            style="opacity:1;vector-effect:none;fill:#53a6b6;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.84952,157.22255 2.21462,-1.19374 -1.2e-4,0.49796 -2.20834,1.21394 z"
                            id="path5548"
                            />
                        </g>
                        <g transform="translate(3.4143682,-1.964431)" id="g5561">
                            <path
                            id="path5555"
                            d="m 100.67708,157.0343 2.5576,-1.37861 0.0137,4.03209 -2.52452,1.53262 z"
                            style="opacity:1;vector-effect:none;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                            <path
                            style="opacity:1;vector-effect:none;fill:#67beda;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.84952,157.22255 2.21462,-1.19374 0.0116,3.49138 -2.18598,1.32709 z"
                            id="path5557"
                            />
                            <path
                            id="path5559"
                            d="m 100.84952,157.22255 2.21462,-1.19374 -1.2e-4,0.49796 -2.20834,1.21394 z"
                            style="opacity:1;vector-effect:none;fill:#53a6b6;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                        </g>
                        <g id="g5569" transform="translate(6.6416477,-3.7885455)">
                            <path
                            style="opacity:1;vector-effect:none;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.67708,157.0343 2.5576,-1.37861 0.0137,4.03209 -2.52452,1.53262 z"
                            id="path5563"
                            />
                            <path
                            id="path5565"
                            d="m 100.84952,157.22255 2.21462,-1.19374 0.0116,3.49138 -2.18598,1.32709 z"
                            style="opacity:1;vector-effect:none;fill:#67beda;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                            <path
                            style="opacity:1;vector-effect:none;fill:#53a6b6;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.84952,157.22255 2.21462,-1.19374 -1.2e-4,0.49796 -2.20834,1.21394 z"
                            id="path5567"
                            />
                        </g>
                        <g transform="translate(6.7585781,1.7773424)" id="g5577">
                            <path
                            id="path5571"
                            d="m 100.67708,157.0343 2.5576,-1.37861 0.0137,4.03209 -2.52452,1.53262 z"
                            style="opacity:1;vector-effect:none;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                            <path
                            style="opacity:1;vector-effect:none;fill:#67beda;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.84952,157.22255 2.21462,-1.19374 0.0116,3.49138 -2.18598,1.32709 z"
                            id="path5573"
                            />
                            <path
                            id="path5575"
                            d="m 100.84952,157.22255 2.21462,-1.19374 -1.2e-4,0.49796 -2.20834,1.21394 z"
                            style="opacity:1;vector-effect:none;fill:#53a6b6;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                        </g>
                        <g id="g5585" transform="translate(3.4377542,3.7417734)">
                            <path
                            style="opacity:1;vector-effect:none;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.67708,157.0343 2.5576,-1.37861 0.0137,4.03209 -2.52452,1.53262 z"
                            id="path5579"
                            />
                            <path
                            id="path5581"
                            d="m 100.84952,157.22255 2.21462,-1.19374 0.0116,3.49138 -2.18598,1.32709 z"
                            style="opacity:1;vector-effect:none;fill:#67beda;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                            <path
                            style="opacity:1;vector-effect:none;fill:#53a6b6;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.84952,157.22255 2.21462,-1.19374 -1.2e-4,0.49796 -2.20834,1.21394 z"
                            id="path5583"
                            />
                        </g>
                        <g transform="translate(0.04677208,5.7295905)" id="g5593">
                            <path
                            id="path5587"
                            d="m 100.67708,157.0343 2.5576,-1.37861 0.0137,4.03209 -2.52452,1.53262 z"
                            style="opacity:1;vector-effect:none;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                            <path
                            style="opacity:1;vector-effect:none;fill:#67beda;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.84952,157.22255 2.21462,-1.19374 0.0116,3.49138 -2.18598,1.32709 z"
                            id="path5589"
                            />
                            <path
                            id="path5591"
                            d="m 100.84952,157.22255 2.21462,-1.19374 -1.2e-4,0.49796 -2.20834,1.21394 z"
                            style="opacity:1;vector-effect:none;fill:#53a6b6;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                        </g>
                        <g transform="matrix(-1,0,0,1,198.80391,0.21047475)" id="g5601">
                            <path
                            id="path5595"
                            d="m 100.67708,157.0343 2.5576,-1.37861 0.0137,4.03209 -2.52452,1.53262 z"
                            style="opacity:1;vector-effect:none;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                            <path
                            style="opacity:1;vector-effect:none;fill:#67beda;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.84952,157.22255 2.21462,-1.19374 0.0116,3.49138 -2.18598,1.32709 z"
                            id="path5597"
                            />
                            <path
                            id="path5599"
                            d="m 100.84952,157.22255 2.21462,-1.19374 -1.2e-4,0.49796 -2.20834,1.21394 z"
                            style="opacity:1;vector-effect:none;fill:#53a6b6;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                        </g>
                        <g id="g5609" transform="matrix(-1,0,0,1,194.7815,-2.1982919)">
                            <path
                            style="opacity:1;vector-effect:none;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.67708,157.0343 2.5576,-1.37861 0.0137,4.03209 -2.52452,1.53262 z"
                            id="path5603"
                            />
                            <path
                            id="path5605"
                            d="m 100.84952,157.22255 2.21462,-1.19374 0.0116,3.49138 -2.18598,1.32709 z"
                            style="opacity:1;vector-effect:none;fill:#67beda;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                            <path
                            style="opacity:1;vector-effect:none;fill:#53a6b6;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.84952,157.22255 2.21462,-1.19374 -1.2e-4,0.49796 -2.20834,1.21394 z"
                            id="path5607"
                            />
                        </g>
                        <g transform="matrix(-1,0,0,1,178.01353,0.7782706)" id="g5617">
                            <path
                            id="path5611"
                            d="m 100.67708,157.0343 2.5576,-1.37861 0.0137,4.03209 -2.52452,1.53262 z"
                            style="opacity:1;vector-effect:none;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                            <path
                            style="opacity:1;vector-effect:none;fill:#67beda;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.84952,157.22255 2.21462,-1.19374 0.0116,3.49138 -2.18598,1.32709 z"
                            id="path5613"
                            />
                            <path
                            id="path5615"
                            d="m 100.84952,157.22255 2.21462,-1.19374 -1.2e-4,0.49796 -2.20834,1.21394 z"
                            style="opacity:1;vector-effect:none;fill:#53a6b6;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                        </g>
                        <g id="g5625" transform="matrix(-1,0,0,1,173.99517,-1.5864429)">
                            <path
                            style="opacity:1;vector-effect:none;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.67708,157.0343 2.5576,-1.37861 0.0137,4.03209 -2.52452,1.53262 z"
                            id="path5619"
                            />
                            <path
                            id="path5621"
                            d="m 100.84952,157.22255 2.21462,-1.19374 0.0116,3.49138 -2.18598,1.32709 z"
                            style="opacity:1;vector-effect:none;fill:#67beda;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                            <path
                            style="opacity:1;vector-effect:none;fill:#53a6b6;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.84952,157.22255 2.21462,-1.19374 -1.2e-4,0.49796 -2.20834,1.21394 z"
                            id="path5623"
                            />
                        </g>
                        <g transform="matrix(-1,0,0,1,170.1091,-3.9015471)" id="g5633">
                            <path
                            id="path5627"
                            d="m 100.67708,157.0343 2.5576,-1.37861 0.0137,4.03209 -2.52452,1.53262 z"
                            style="opacity:1;vector-effect:none;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                            <path
                            style="opacity:1;vector-effect:none;fill:#67beda;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.84952,157.22255 2.21462,-1.19374 0.0116,3.49138 -2.18598,1.32709 z"
                            id="path5629"
                            />
                            <path
                            id="path5631"
                            d="m 100.84952,157.22255 2.21462,-1.19374 -1.2e-4,0.49796 -2.20834,1.21394 z"
                            style="opacity:1;vector-effect:none;fill:#53a6b6;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                        </g>
                        </g>
                        <g style="display:inline" id="g5524">
                        <g id="g5513">
                            <path
                            id="path5488"
                            d="m 65.513537,147.79703 13.766963,8.27638 11.358957,-6.71414 8.952857,5.30963 11.194126,-6.52974 -22.723897,-13.29568 z"
                            style="display:inline;fill:#d0c38e;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                            />
                            <path
                            id="path5492"
                            d="m 88.062322,134.84345 -22.549008,12.95373 13.767117,8.27598 11.358996,-6.7138 8.952942,5.30975 11.194151,-6.52983 z m -0.130741,1.59421 20.713469,12.16102 -8.953462,5.07824 -8.886796,-5.07824 -11.358996,6.61458 -11.504724,-6.87038 z"
                            style="fill:#f3e8d0;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                            />
                            <path
                            id="path5486"
                            d="m 108.64506,148.59859 v 0 l -20.713386,-12.16076 -19.99062,11.90518 -1.00538,-0.54598 20.961506,-12.19287 21.5002,12.56807 z"
                            style="display:inline;fill:#cbb76f;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                            />
                        </g>
                        <g id="g5508">
                            <path
                            id="path5496"
                            d="m 65.513314,147.79718 13.767115,8.27598 -0.0045,1.43001 -13.890743,-8.20682 z"
                            style="fill:#ecead1;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                            />
                            <path
                            id="path5498"
                            d="m 79.280498,156.07341 -0.0046,1.42976 11.272999,-6.61459 0.09051,-1.52922 z"
                            style="fill:#d8d3ac;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                            />
                            <path
                            id="path5500"
                            d="m 90.639425,149.35936 -0.09051,1.52923 9.000559,5.0318 0.04284,-1.25149 z"
                            style="fill:#efe5d3;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                            />
                            <path
                            id="path5502"
                            d="m 99.592367,154.66911 -0.04289,1.25128 11.277863,-6.40079 -0.0408,-1.38032 z"
                            style="fill:#ddd1ae;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                            />
                        </g>
                        </g>
                    </g>
                    <g
                        transform="translate(-8.995847,-40.105052)"
                        id="g5541-6"
                        style="display:inline"
                    >
                        <g style="display:inline" id="g5482-9">
                        <path
                            style="opacity:1;vector-effect:none;fill:#c4e1e7;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 65.985009,149.66659 13.29092,7.83658 0.04178,13.03428 -13.416224,-7.75568 z"
                            id="rect5465-1"
                        />
                        <path
                            style="opacity:1;vector-effect:none;fill:#9dc7d3;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 79.275929,157.50317 10.688334,-7.0823 0.06493,13.98037 -10.711489,6.13621 z"
                            id="rect5468-9"
                        />
                        <path
                            style="fill:#c1dada;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                            d="m 89.964263,150.42087 0.06493,13.98037 9.425789,5.50426 0.09449,-13.98511 z"
                            id="path5471-4"
                        />
                        <path
                            style="fill:#98c2d3;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                            d="m 99.549473,155.92039 -0.09449,13.98511 10.866817,-6.2366 -0.0472,-13.84338 z"
                            id="path5473-8"
                        />
                        </g>
                        <g id="g6185-9">
                        <path
                            id="rect6165-1"
                            d="m 84.013474,160.7644 3.395405,-1.56236 -0.0065,6.70471 -3.263854,1.8691 z"
                            style="display:inline;opacity:1;vector-effect:none;fill:#7e4a3d;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                        />
                        <path
                            id="rect6168-9"
                            d="m 84.696069,161.3055 0.97707,-0.41709 -0.0248,2.62929 -0.96797,0.4505 z"
                            style="opacity:1;vector-effect:none;fill:#a07062;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                        />
                        <path
                            style="opacity:1;vector-effect:none;fill:#a07062;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 85.970611,160.74423 0.961121,-0.41224 -0.0089,2.62444 -0.979663,0.43881 z"
                            id="path6171-8"
                        />
                        <path
                            style="opacity:1;vector-effect:none;fill:#a07062;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 84.691935,164.29446 0.949428,-0.43563 0.0028,2.64783 -0.979663,0.56744 z"
                            id="path6173-2"
                        />
                        <path
                            id="path6175-1"
                            d="m 85.966477,163.73319 0.937735,-0.40799 -0.02055,2.5208 -0.937172,0.51653 z"
                            style="opacity:1;vector-effect:none;fill:#a07062;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                        />
                        <circle
                            r="0.12461104"
                            cy="163.08266"
                            cx="87.142395"
                            id="path6177-8"
                            style="opacity:1;vector-effect:none;fill:#a07062;fill-opacity:1;stroke:none;stroke-width:1.06568539;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                        />
                        </g>
                        <g id="g5679-2">
                        <g id="g5553-2">
                            <path
                            style="opacity:1;vector-effect:none;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.67708,157.0343 2.5576,-1.37861 0.0137,4.03209 -2.52452,1.53262 z"
                            id="rect5543-2"
                            />
                            <path
                            id="path5546-0"
                            d="m 100.84952,157.22255 2.21462,-1.19374 0.0116,3.49138 -2.18598,1.32709 z"
                            style="opacity:1;vector-effect:none;fill:#67beda;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                            <path
                            style="opacity:1;vector-effect:none;fill:#53a6b6;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.84952,157.22255 2.21462,-1.19374 -1.2e-4,0.49796 -2.20834,1.21394 z"
                            id="path5548-2"
                            />
                        </g>
                        <g transform="translate(3.4143682,-1.964431)" id="g5561-4">
                            <path
                            id="path5555-3"
                            d="m 100.67708,157.0343 2.5576,-1.37861 0.0137,4.03209 -2.52452,1.53262 z"
                            style="opacity:1;vector-effect:none;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                            <path
                            style="opacity:1;vector-effect:none;fill:#67beda;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.84952,157.22255 2.21462,-1.19374 0.0116,3.49138 -2.18598,1.32709 z"
                            id="path5557-3"
                            />
                            <path
                            id="path5559-8"
                            d="m 100.84952,157.22255 2.21462,-1.19374 -1.2e-4,0.49796 -2.20834,1.21394 z"
                            style="opacity:1;vector-effect:none;fill:#53a6b6;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                        </g>
                        <g id="g5569-3" transform="translate(6.6416477,-3.7885455)">
                            <path
                            style="opacity:1;vector-effect:none;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.67708,157.0343 2.5576,-1.37861 0.0137,4.03209 -2.52452,1.53262 z"
                            id="path5563-0"
                            />
                            <path
                            id="path5565-9"
                            d="m 100.84952,157.22255 2.21462,-1.19374 0.0116,3.49138 -2.18598,1.32709 z"
                            style="opacity:1;vector-effect:none;fill:#67beda;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                            <path
                            style="opacity:1;vector-effect:none;fill:#53a6b6;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.84952,157.22255 2.21462,-1.19374 -1.2e-4,0.49796 -2.20834,1.21394 z"
                            id="path5567-5"
                            />
                        </g>
                        <g transform="translate(6.7585781,1.7773424)" id="g5577-9">
                            <path
                            id="path5571-5"
                            d="m 100.67708,157.0343 2.5576,-1.37861 0.0137,4.03209 -2.52452,1.53262 z"
                            style="opacity:1;vector-effect:none;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                            <path
                            style="opacity:1;vector-effect:none;fill:#67beda;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.84952,157.22255 2.21462,-1.19374 0.0116,3.49138 -2.18598,1.32709 z"
                            id="path5573-1"
                            />
                            <path
                            id="path5575-9"
                            d="m 100.84952,157.22255 2.21462,-1.19374 -1.2e-4,0.49796 -2.20834,1.21394 z"
                            style="opacity:1;vector-effect:none;fill:#53a6b6;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                        </g>
                        <g id="g5585-6" transform="translate(3.4377542,3.7417734)">
                            <path
                            style="opacity:1;vector-effect:none;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.67708,157.0343 2.5576,-1.37861 0.0137,4.03209 -2.52452,1.53262 z"
                            id="path5579-6"
                            />
                            <path
                            id="path5581-1"
                            d="m 100.84952,157.22255 2.21462,-1.19374 0.0116,3.49138 -2.18598,1.32709 z"
                            style="opacity:1;vector-effect:none;fill:#67beda;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                            <path
                            style="opacity:1;vector-effect:none;fill:#53a6b6;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.84952,157.22255 2.21462,-1.19374 -1.2e-4,0.49796 -2.20834,1.21394 z"
                            id="path5583-3"
                            />
                        </g>
                        <g transform="translate(0.04677208,5.7295905)" id="g5593-4">
                            <path
                            id="path5587-9"
                            d="m 100.67708,157.0343 2.5576,-1.37861 0.0137,4.03209 -2.52452,1.53262 z"
                            style="opacity:1;vector-effect:none;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                            <path
                            style="opacity:1;vector-effect:none;fill:#67beda;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.84952,157.22255 2.21462,-1.19374 0.0116,3.49138 -2.18598,1.32709 z"
                            id="path5589-6"
                            />
                            <path
                            id="path5591-6"
                            d="m 100.84952,157.22255 2.21462,-1.19374 -1.2e-4,0.49796 -2.20834,1.21394 z"
                            style="opacity:1;vector-effect:none;fill:#53a6b6;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                        </g>
                        <g transform="matrix(-1,0,0,1,198.80391,0.21047475)" id="g5601-3">
                            <path
                            id="path5595-8"
                            d="m 100.67708,157.0343 2.5576,-1.37861 0.0137,4.03209 -2.52452,1.53262 z"
                            style="opacity:1;vector-effect:none;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                            <path
                            style="opacity:1;vector-effect:none;fill:#67beda;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.84952,157.22255 2.21462,-1.19374 0.0116,3.49138 -2.18598,1.32709 z"
                            id="path5597-4"
                            />
                            <path
                            id="path5599-5"
                            d="m 100.84952,157.22255 2.21462,-1.19374 -1.2e-4,0.49796 -2.20834,1.21394 z"
                            style="opacity:1;vector-effect:none;fill:#53a6b6;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                        </g>
                        <g id="g5609-0" transform="matrix(-1,0,0,1,194.7815,-2.1982919)">
                            <path
                            style="opacity:1;vector-effect:none;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.67708,157.0343 2.5576,-1.37861 0.0137,4.03209 -2.52452,1.53262 z"
                            id="path5603-4"
                            />
                            <path
                            id="path5605-3"
                            d="m 100.84952,157.22255 2.21462,-1.19374 0.0116,3.49138 -2.18598,1.32709 z"
                            style="opacity:1;vector-effect:none;fill:#67beda;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                            <path
                            style="opacity:1;vector-effect:none;fill:#53a6b6;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.84952,157.22255 2.21462,-1.19374 -1.2e-4,0.49796 -2.20834,1.21394 z"
                            id="path5607-5"
                            />
                        </g>
                        <g transform="matrix(-1,0,0,1,178.01353,0.7782706)" id="g5617-0">
                            <path
                            id="path5611-7"
                            d="m 100.67708,157.0343 2.5576,-1.37861 0.0137,4.03209 -2.52452,1.53262 z"
                            style="opacity:1;vector-effect:none;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                            <path
                            style="opacity:1;vector-effect:none;fill:#67beda;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.84952,157.22255 2.21462,-1.19374 0.0116,3.49138 -2.18598,1.32709 z"
                            id="path5613-9"
                            />
                            <path
                            id="path5615-7"
                            d="m 100.84952,157.22255 2.21462,-1.19374 -1.2e-4,0.49796 -2.20834,1.21394 z"
                            style="opacity:1;vector-effect:none;fill:#53a6b6;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                        </g>
                        <g id="g5625-0" transform="matrix(-1,0,0,1,173.99517,-1.5864429)">
                            <path
                            style="opacity:1;vector-effect:none;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.67708,157.0343 2.5576,-1.37861 0.0137,4.03209 -2.52452,1.53262 z"
                            id="path5619-4"
                            />
                            <path
                            id="path5621-2"
                            d="m 100.84952,157.22255 2.21462,-1.19374 0.0116,3.49138 -2.18598,1.32709 z"
                            style="opacity:1;vector-effect:none;fill:#67beda;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                            <path
                            style="opacity:1;vector-effect:none;fill:#53a6b6;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.84952,157.22255 2.21462,-1.19374 -1.2e-4,0.49796 -2.20834,1.21394 z"
                            id="path5623-1"
                            />
                        </g>
                        <g transform="matrix(-1,0,0,1,170.1091,-3.9015471)" id="g5633-6">
                            <path
                            id="path5627-7"
                            d="m 100.67708,157.0343 2.5576,-1.37861 0.0137,4.03209 -2.52452,1.53262 z"
                            style="opacity:1;vector-effect:none;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                            <path
                            style="opacity:1;vector-effect:none;fill:#67beda;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.84952,157.22255 2.21462,-1.19374 0.0116,3.49138 -2.18598,1.32709 z"
                            id="path5629-0"
                            />
                            <path
                            id="path5631-7"
                            d="m 100.84952,157.22255 2.21462,-1.19374 -1.2e-4,0.49796 -2.20834,1.21394 z"
                            style="opacity:1;vector-effect:none;fill:#53a6b6;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                        </g>
                        </g>
                        <g style="display:inline" id="g5524-0">
                        <g id="g5513-3">
                            <path
                            id="path5488-3"
                            d="m 65.513537,147.79703 13.766963,8.27638 11.358957,-6.71414 8.952857,5.30963 11.194126,-6.52974 -22.723897,-13.29568 z"
                            style="display:inline;fill:#163a4a;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                            />
                            <path
                            id="path5492-8"
                            d="m 88.062322,134.84345 -22.549008,12.95373 13.767117,8.27598 11.358996,-6.7138 8.952942,5.30975 11.194151,-6.52983 z m -0.130741,1.59421 20.713469,12.16102 -8.953462,5.07824 -8.886796,-5.07824 -11.358996,6.61458 -11.504724,-6.87038 z"
                            style="fill:#cbe8d0;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                            />
                            <path
                            id="path5486-9"
                            d="m 108.64506,148.59859 -0.10432,0.0621 -20.609169,-11.85917 -19.916113,11.58289 -1.079794,-0.58732 20.961506,-12.19287 21.5002,12.56807 z"
                            style="display:inline;fill:#0d2934;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                            />
                        </g>
                        <g id="g5508-4">
                            <path
                            id="path5496-1"
                            d="m 65.513314,147.79718 13.767115,8.27598 -0.0045,1.43001 -13.890743,-8.20682 z"
                            style="fill:#c5ead1;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                            />
                            <path
                            id="path5498-5"
                            d="m 79.280498,156.07341 -0.0046,1.42976 11.272999,-6.61459 0.09051,-1.52922 z"
                            style="fill:#b0d3ac;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                            />
                            <path
                            id="path5500-2"
                            d="m 90.639425,149.35936 -0.09051,1.52923 9.000559,5.0318 0.04284,-1.25149 z"
                            style="fill:#c7e5d3;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                            />
                            <path
                            id="path5502-9"
                            d="m 99.592367,154.66911 -0.04289,1.25128 11.277863,-6.40079 -0.0408,-1.38032 z"
                            style="fill:#b5d1ae;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                            />
                        </g>
                        </g>
                    </g>
                    <g
                        style="display:inline"
                        id="g6558"
                        transform="translate(56.605584,-20.639894)"
                    >
                        <g id="g6432" style="display:inline">
                        <path
                            id="path6424"
                            d="m 65.985009,149.66659 13.29092,7.83658 0.04178,13.03428 -13.416224,-7.75568 z"
                            style="opacity:1;vector-effect:none;fill:#eb8e2f;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                        />
                        <path
                            id="path6426"
                            d="m 79.275929,157.50317 10.688334,-7.0823 0.06493,13.98037 -10.711489,6.13621 z"
                            style="opacity:1;vector-effect:none;fill:#eb9e33;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                        />
                        <path
                            id="path6428"
                            d="m 89.964263,150.42087 0.06493,13.98037 9.425789,5.50426 0.09449,-13.98511 z"
                            style="fill:#eb8f2e;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                        />
                        <path
                            id="path6430"
                            d="m 99.549473,155.92039 -0.09449,13.98511 10.866817,-6.2366 -0.0472,-13.84338 z"
                            style="fill:#ed9d36;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                        />
                        </g>
                        <g id="g6446">
                        <path
                            style="display:inline;opacity:1;vector-effect:none;fill:#7e4a3d;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 84.013474,160.7644 3.395405,-1.56236 -0.0065,6.70471 -3.263854,1.8691 z"
                            id="path6434"
                        />
                        <path
                            style="opacity:1;vector-effect:none;fill:#a07062;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 84.696069,161.3055 0.97707,-0.41709 -0.0248,2.62929 -0.96797,0.4505 z"
                            id="path6436"
                        />
                        <path
                            id="path6438"
                            d="m 85.970611,160.74423 0.961121,-0.41224 -0.0089,2.62444 -0.979663,0.43881 z"
                            style="opacity:1;vector-effect:none;fill:#a07062;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                        />
                        <path
                            id="path6440"
                            d="m 84.691935,164.29446 0.949428,-0.43563 0.0028,2.64783 -0.979663,0.56744 z"
                            style="opacity:1;vector-effect:none;fill:#a07062;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                        />
                        <path
                            style="opacity:1;vector-effect:none;fill:#a07062;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 85.966477,163.73319 0.937735,-0.40799 -0.02055,2.5208 -0.937172,0.51653 z"
                            id="path6442"
                        />
                        <circle
                            style="opacity:1;vector-effect:none;fill:#a07062;fill-opacity:1;stroke:none;stroke-width:1.06568539;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            id="circle6444"
                            cx="87.142395"
                            cy="163.08266"
                            r="0.12461104"
                        />
                        </g>
                        <g id="g6536">
                        <g id="g6454">
                            <path
                            id="path6448"
                            d="m 100.67708,157.0343 2.5576,-1.37861 0.0137,4.03209 -2.52452,1.53262 z"
                            style="opacity:1;vector-effect:none;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                            <path
                            style="opacity:1;vector-effect:none;fill:#67beda;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.84952,157.22255 2.21462,-1.19374 0.0116,3.49138 -2.18598,1.32709 z"
                            id="path6450"
                            />
                            <path
                            id="path6452"
                            d="m 100.84952,157.22255 2.21462,-1.19374 -1.2e-4,0.49796 -2.20834,1.21394 z"
                            style="opacity:1;vector-effect:none;fill:#53a6b6;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                        </g>
                        <g id="g6462" transform="translate(3.4143682,-1.964431)">
                            <path
                            style="opacity:1;vector-effect:none;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.67708,157.0343 2.5576,-1.37861 0.0137,4.03209 -2.52452,1.53262 z"
                            id="path6456"
                            />
                            <path
                            id="path6458"
                            d="m 100.84952,157.22255 2.21462,-1.19374 0.0116,3.49138 -2.18598,1.32709 z"
                            style="opacity:1;vector-effect:none;fill:#67beda;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                            <path
                            style="opacity:1;vector-effect:none;fill:#53a6b6;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.84952,157.22255 2.21462,-1.19374 -1.2e-4,0.49796 -2.20834,1.21394 z"
                            id="path6460"
                            />
                        </g>
                        <g transform="translate(6.6416477,-3.7885455)" id="g6470">
                            <path
                            id="path6464"
                            d="m 100.67708,157.0343 2.5576,-1.37861 0.0137,4.03209 -2.52452,1.53262 z"
                            style="opacity:1;vector-effect:none;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                            <path
                            style="opacity:1;vector-effect:none;fill:#67beda;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.84952,157.22255 2.21462,-1.19374 0.0116,3.49138 -2.18598,1.32709 z"
                            id="path6466"
                            />
                            <path
                            id="path6468"
                            d="m 100.84952,157.22255 2.21462,-1.19374 -1.2e-4,0.49796 -2.20834,1.21394 z"
                            style="opacity:1;vector-effect:none;fill:#53a6b6;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                        </g>
                        <g id="g6478" transform="translate(6.7585781,1.7773424)">
                            <path
                            style="opacity:1;vector-effect:none;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.67708,157.0343 2.5576,-1.37861 0.0137,4.03209 -2.52452,1.53262 z"
                            id="path6472"
                            />
                            <path
                            id="path6474"
                            d="m 100.84952,157.22255 2.21462,-1.19374 0.0116,3.49138 -2.18598,1.32709 z"
                            style="opacity:1;vector-effect:none;fill:#67beda;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                            <path
                            style="opacity:1;vector-effect:none;fill:#53a6b6;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.84952,157.22255 2.21462,-1.19374 -1.2e-4,0.49796 -2.20834,1.21394 z"
                            id="path6476"
                            />
                        </g>
                        <g transform="translate(3.4377542,3.7417734)" id="g6486">
                            <path
                            id="path6480"
                            d="m 100.67708,157.0343 2.5576,-1.37861 0.0137,4.03209 -2.52452,1.53262 z"
                            style="opacity:1;vector-effect:none;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                            <path
                            style="opacity:1;vector-effect:none;fill:#67beda;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.84952,157.22255 2.21462,-1.19374 0.0116,3.49138 -2.18598,1.32709 z"
                            id="path6482"
                            />
                            <path
                            id="path6484"
                            d="m 100.84952,157.22255 2.21462,-1.19374 -1.2e-4,0.49796 -2.20834,1.21394 z"
                            style="opacity:1;vector-effect:none;fill:#53a6b6;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                        </g>
                        <g id="g6494" transform="translate(0.04677208,5.7295905)">
                            <path
                            style="opacity:1;vector-effect:none;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.67708,157.0343 2.5576,-1.37861 0.0137,4.03209 -2.52452,1.53262 z"
                            id="path6488"
                            />
                            <path
                            id="path6490"
                            d="m 100.84952,157.22255 2.21462,-1.19374 0.0116,3.49138 -2.18598,1.32709 z"
                            style="opacity:1;vector-effect:none;fill:#67beda;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                            <path
                            style="opacity:1;vector-effect:none;fill:#53a6b6;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.84952,157.22255 2.21462,-1.19374 -1.2e-4,0.49796 -2.20834,1.21394 z"
                            id="path6492"
                            />
                        </g>
                        <g id="g6502" transform="matrix(-1,0,0,1,198.80391,0.21047475)">
                            <path
                            style="opacity:1;vector-effect:none;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.67708,157.0343 2.5576,-1.37861 0.0137,4.03209 -2.52452,1.53262 z"
                            id="path6496"
                            />
                            <path
                            id="path6498"
                            d="m 100.84952,157.22255 2.21462,-1.19374 0.0116,3.49138 -2.18598,1.32709 z"
                            style="opacity:1;vector-effect:none;fill:#67beda;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                            <path
                            style="opacity:1;vector-effect:none;fill:#53a6b6;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.84952,157.22255 2.21462,-1.19374 -1.2e-4,0.49796 -2.20834,1.21394 z"
                            id="path6500"
                            />
                        </g>
                        <g transform="matrix(-1,0,0,1,194.7815,-2.1982919)" id="g6510">
                            <path
                            id="path6504"
                            d="m 100.67708,157.0343 2.5576,-1.37861 0.0137,4.03209 -2.52452,1.53262 z"
                            style="opacity:1;vector-effect:none;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                            <path
                            style="opacity:1;vector-effect:none;fill:#67beda;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.84952,157.22255 2.21462,-1.19374 0.0116,3.49138 -2.18598,1.32709 z"
                            id="path6506"
                            />
                            <path
                            id="path6508"
                            d="m 100.84952,157.22255 2.21462,-1.19374 -1.2e-4,0.49796 -2.20834,1.21394 z"
                            style="opacity:1;vector-effect:none;fill:#53a6b6;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                        </g>
                        <g id="g6518" transform="matrix(-1,0,0,1,178.01353,0.7782706)">
                            <path
                            style="opacity:1;vector-effect:none;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.67708,157.0343 2.5576,-1.37861 0.0137,4.03209 -2.52452,1.53262 z"
                            id="path6512"
                            />
                            <path
                            id="path6514"
                            d="m 100.84952,157.22255 2.21462,-1.19374 0.0116,3.49138 -2.18598,1.32709 z"
                            style="opacity:1;vector-effect:none;fill:#67beda;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                            <path
                            style="opacity:1;vector-effect:none;fill:#53a6b6;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.84952,157.22255 2.21462,-1.19374 -1.2e-4,0.49796 -2.20834,1.21394 z"
                            id="path6516"
                            />
                        </g>
                        <g transform="matrix(-1,0,0,1,173.99517,-1.5864429)" id="g6526">
                            <path
                            id="path6520"
                            d="m 100.67708,157.0343 2.5576,-1.37861 0.0137,4.03209 -2.52452,1.53262 z"
                            style="opacity:1;vector-effect:none;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                            <path
                            style="opacity:1;vector-effect:none;fill:#67beda;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.84952,157.22255 2.21462,-1.19374 0.0116,3.49138 -2.18598,1.32709 z"
                            id="path6522"
                            />
                            <path
                            id="path6524"
                            d="m 100.84952,157.22255 2.21462,-1.19374 -1.2e-4,0.49796 -2.20834,1.21394 z"
                            style="opacity:1;vector-effect:none;fill:#53a6b6;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                        </g>
                        <g id="g6534" transform="matrix(-1,0,0,1,170.1091,-3.9015471)">
                            <path
                            style="opacity:1;vector-effect:none;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.5;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.67708,157.0343 2.5576,-1.37861 0.0137,4.03209 -2.52452,1.53262 z"
                            id="path6528"
                            />
                            <path
                            id="path6530"
                            d="m 100.84952,157.22255 2.21462,-1.19374 0.0116,3.49138 -2.18598,1.32709 z"
                            style="opacity:1;vector-effect:none;fill:#67beda;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            />
                            <path
                            style="opacity:1;vector-effect:none;fill:#53a6b6;fill-opacity:1;stroke:none;stroke-width:0.43294817;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 100.84952,157.22255 2.21462,-1.19374 -1.2e-4,0.49796 -2.20834,1.21394 z"
                            id="path6532"
                            />
                        </g>
                        </g>
                        <g id="g6556" style="display:inline">
                        <g id="g6544">
                            <path
                            style="display:inline;fill:#5a281f;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                            d="m 65.513537,147.79703 13.766963,8.27638 11.358957,-6.71414 8.952857,5.30963 11.194126,-6.52974 -22.723897,-13.29568 z"
                            id="path6538"
                            />
                            <path
                            style="fill:#c9baaa;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                            d="m 88.062322,134.84345 -22.549008,12.95373 13.767117,8.27598 11.358996,-6.7138 8.952942,5.30975 11.194151,-6.52983 z m -0.130741,1.59421 20.713469,12.16102 -8.953462,5.07824 -8.886796,-5.07824 -11.358996,6.61458 -11.504724,-6.87038 z"
                            id="path6540"
                            />
                            <path
                            style="display:inline;fill:#471c15;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                            d="m 108.53017,148.66381 -20.598589,-12.12592 -19.879443,11.86992 -1.116464,-0.61078 20.961506,-12.19287 21.5002,12.56807 z"
                            id="path6542"
                            />
                        </g>
                        <g id="g6554">
                            <path
                            style="fill:#e7dcc9;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                            d="m 65.513314,147.79718 13.767115,8.27598 -0.0045,1.43001 -13.890743,-8.20682 z"
                            id="path6546"
                            />
                            <path
                            style="fill:#cebfac;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                            d="m 79.280498,156.07341 -0.0046,1.42976 11.272999,-6.61459 0.09051,-1.52922 z"
                            id="path6548"
                            />
                            <path
                            style="fill:#efe5d3;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                            d="m 90.639425,149.35936 -0.09051,1.52923 9.000559,5.0318 0.04284,-1.25149 z"
                            id="path6550"
                            />
                            <path
                            style="fill:#c7bfa2;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                            d="m 99.592367,154.66911 -0.04289,1.25128 11.277863,-6.40079 -0.0408,-1.38032 z"
                            id="path6552"
                            />
                        </g>
                        </g>
                    </g>
                    <g id="g7153" style="display:inline">
                        <g id="g7133">
                        <path
                            id="rect6568"
                            transform="scale(0.26458333)"
                            style="display:inline;opacity:1;vector-effect:none;fill:#794327;fill-opacity:1;stroke:none;stroke-width:1.60390437;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 136.47461,491.33789 c -0.98152,0 -1.77149,0.78997 -1.77149,1.77149 v 36.32031 c 0,0.32623 0.0989,0.62488 0.25196,0.88672 0.0244,-0.0205 0.0482,-0.0419 0.0723,-0.0625 -0.0113,0.004 -0.0422,0.0236 -0.0332,0.0156 0.0867,-0.0733 0.16743,-0.15424 0.26563,-0.21094 0.10801,-0.0626 0.24118,-0.0929 0.37695,-0.10547 0.13577,-0.0124 0.27438,-0.008 0.39453,0.006 0.1245,0.034 0.25531,-0.003 0.38086,0.0117 0.15224,0.0189 0.29311,0.0603 0.43945,0.0996 0.13285,0.008 0.25211,0.0767 0.38086,0.10352 0.0136,0.003 0.043,-0.0155 0.0625,-0.0195 0.0333,-0.004 0.0662,-0.006 0.0996,-0.01 0.18093,-0.0119 0.27869,0.0221 0.39258,0.084 0.0612,-7.7e-4 0.12243,-0.002 0.18359,-0.002 0.17809,0.002 0.35511,1.9e-4 0.53321,0 0.0234,1e-5 0.0469,0 0.0703,0 0.12122,-0.24015 0.19531,-0.50851 0.19531,-0.79687 v -36.32031 c 0,-0.98152 -0.78996,-1.77149 -1.77148,-1.77149 z"
                        />
                        <path
                            id="rect6566"
                            transform="scale(0.26458333)"
                            style="display:inline;opacity:1;vector-effect:none;fill:url(#linearGradient7108);fill-opacity:1;stroke:none;stroke-width:1.29526961;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 136.0293,491.33789 c -0.73451,0 -1.32618,0.78997 -1.32618,1.77149 v 36.32031 c 0,0.25334 0.0422,0.49339 0.11329,0.71093 h 0.31836 c 0.0663,0 0.13341,-0.008 0.19921,0 0.0967,0.0113 0.18966,0.0377 0.28516,0.0566 0.0254,0.006 0.051,0.0125 0.0762,0.0195 0.0276,-0.005 0.055,-0.005 0.0859,-0.01 0.25123,-0.0378 0.46664,-0.0199 0.66797,0.15625 -0.0254,-0.0113 -0.10097,-0.0425 -0.0762,-0.0293 0.0527,0.0265 0.10815,0.0451 0.16015,0.0723 0.0242,0.0114 0.0624,0.0197 0.0664,0.0469 0.003,0.0185 -0.0769,0.0215 -0.0566,0.0215 0.0575,0 0.11487,-0.0135 0.17187,-0.0215 0.0726,0.004 0.1441,0.008 0.2168,0.0117 0.0485,0.014 0.0975,0.0263 0.14648,0.0391 0.17113,-0.29838 0.27735,-0.66758 0.27735,-1.07422 v -36.32031 c 0,-0.98152 -0.59167,-1.77149 -1.32617,-1.77149 z"
                        />
                        </g>
                        <g id="g7137">
                        <path
                            style="fill:#5d973c;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                            d="m 33.023307,129.79987 c 0.165809,0.98597 0.785387,0.3263 1.422622,0.64492 0.637235,0.31862 0.705217,0.28291 1.157066,0.1323 0.451849,-0.15062 0.276133,-0.33074 0.843359,-0.33074 0.567226,0 0.726157,0.39132 1.352951,0.18239 0.626794,-0.20893 0.45413,-0.13819 1.16059,-0.57926 0.70646,-0.44107 1.052781,-0.96643 1.372527,-1.71979 0.319746,-0.75336 0.693698,-0.95426 0.464825,-2.0083 -0.228873,-1.05405 -0.11062,-1.37489 -0.712872,-1.87777 -0.602252,-0.50288 -0.446893,-0.98285 -0.856917,-1.39287 -0.410023,-0.41002 -0.799047,-0.32699 -1.308671,-0.70256 -0.509623,-0.37556 -1.391401,-0.55473 -2.157278,-0.48945 -0.765877,0.0653 -1.482282,0.43279 -2.035164,0.96881 -0.552882,0.53602 -0.680309,0.10105 -1.452857,0.87359 -0.772548,0.77253 -0.323878,0.99008 -0.523492,1.85042 -0.199614,0.86034 -0.714776,1.09126 -0.33073,1.78594 0.384046,0.69468 -0.03042,0.84563 0.479558,1.48828 0.509976,0.64264 0.958674,0.18811 1.124483,1.17409 z"
                            id="path6562"
                        />
                        <path
                            id="path6564"
                            d="m 32.901261,129.32272 c 0.154196,0.92827 0.730381,0.30721 1.322986,0.60718 0.592605,0.29998 0.655826,0.26636 1.076029,0.12456 0.420203,-0.1418 0.256793,-0.31139 0.784293,-0.31139 0.527499,0 0.675299,0.36842 1.258194,0.17172 0.582895,-0.1967 0.422324,-0.1301 1.079306,-0.54536 0.656981,-0.41526 0.979048,-0.90989 1.276399,-1.61917 0.297352,-0.70927 0.645114,-0.89841 0.43227,-1.89078 -0.212843,-0.99237 -0.102876,-1.29443 -0.662944,-1.76789 -0.560073,-0.47345 -0.415595,-0.92533 -0.796901,-1.31136 -0.381307,-0.38603 -0.743084,-0.30786 -1.217016,-0.66145 -0.47393,-0.35359 -1.293951,-0.52227 -2.006189,-0.46081 -0.712238,0.0615 -1.378467,0.40747 -1.892628,0.91212 -0.51416,0.50465 -0.632661,0.0951 -1.351102,0.82246 -0.718441,0.72734 -0.301195,0.93216 -0.486829,1.74215 -0.185634,0.81 -0.664715,1.0274 -0.307567,1.68143 0.357149,0.65404 -0.02829,0.79616 0.445972,1.4012 0.474259,0.60504 0.891531,0.17711 1.045727,1.10539 z"
                            style="fill:#487b2e;fill-opacity:1;stroke:none;stroke-width:0.24757226px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                        />
                        </g>
                    </g>
                    <g id="g7171">
                        <g
                        style="display:inline"
                        transform="matrix(0.50816809,0,0,0.75105971,26.326383,30.60931)"
                        id="g7143"
                        >
                        <path
                            d="m 136.47461,491.33789 c -0.98152,0 -1.77149,0.78997 -1.77149,1.77149 v 36.32031 c 0,0.32623 0.0989,0.62488 0.25196,0.88672 0.0244,-0.0205 0.0482,-0.0419 0.0723,-0.0625 -0.0113,0.004 -0.0422,0.0236 -0.0332,0.0156 0.0867,-0.0733 0.16743,-0.15424 0.26563,-0.21094 0.10801,-0.0626 0.24118,-0.0929 0.37695,-0.10547 0.13577,-0.0124 0.27438,-0.008 0.39453,0.006 0.1245,0.034 0.25531,-0.003 0.38086,0.0117 0.15224,0.0189 0.29311,0.0603 0.43945,0.0996 0.13285,0.008 0.25211,0.0767 0.38086,0.10352 0.0136,0.003 0.043,-0.0155 0.0625,-0.0195 0.0333,-0.004 0.0662,-0.006 0.0996,-0.01 0.18093,-0.0119 0.27869,0.0221 0.39258,0.084 0.0612,-7.7e-4 0.12243,-0.002 0.18359,-0.002 0.17809,0.002 0.35511,1.9e-4 0.53321,0 0.0234,1e-5 0.0469,0 0.0703,0 0.12122,-0.24015 0.19531,-0.50851 0.19531,-0.79687 v -36.32031 c 0,-0.98152 -0.78996,-1.77149 -1.77148,-1.77149 z"
                            style="display:inline;opacity:1;vector-effect:none;fill:#794327;fill-opacity:1;stroke:none;stroke-width:1.60390437;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            transform="scale(0.26458333)"
                            id="path7139"
                        />
                        <path
                            d="m 136.0293,491.33789 c -0.73451,0 -1.32618,0.78997 -1.32618,1.77149 v 36.32031 c 0,0.25334 0.0422,0.49339 0.11329,0.71093 h 0.31836 c 0.0663,0 0.13341,-0.008 0.19921,0 0.0967,0.0113 0.18966,0.0377 0.28516,0.0566 0.0254,0.006 0.051,0.0125 0.0762,0.0195 0.0276,-0.005 0.055,-0.005 0.0859,-0.01 0.25123,-0.0378 0.46664,-0.0199 0.66797,0.15625 -0.0254,-0.0113 -0.10097,-0.0425 -0.0762,-0.0293 0.0527,0.0265 0.10815,0.0451 0.16015,0.0723 0.0242,0.0114 0.0624,0.0197 0.0664,0.0469 0.003,0.0185 -0.0769,0.0215 -0.0566,0.0215 0.0575,0 0.11487,-0.0135 0.17187,-0.0215 0.0726,0.004 0.1441,0.008 0.2168,0.0117 0.0485,0.014 0.0975,0.0263 0.14648,0.0391 0.17113,-0.29838 0.27735,-0.66758 0.27735,-1.07422 v -36.32031 c 0,-0.98152 -0.59167,-1.77149 -1.32617,-1.77149 z"
                            style="display:inline;opacity:1;vector-effect:none;fill:url(#linearGradient7145);fill-opacity:1;stroke:none;stroke-width:1.29526961;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            transform="scale(0.26458333)"
                            id="path7141"
                        />
                        </g>
                        <g id="g7163">
                        <path
                            style="fill:#5a963a;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                            d="m 44.946093,128.26198 c 0.105839,0.10626 0.584229,0.48519 0.727604,0.19844 0.02863,-0.0573 0.01159,-0.14387 0.06615,-0.19844 0.05761,-0.0576 0.181483,0.17342 0.231511,0.19844 0.02201,0.011 0.325225,0.011 0.330729,0 0.03371,-0.0674 -0.02548,-0.21877 0.06615,-0.26459 0.13984,-0.0699 0.456475,0.23806 0.595312,0.0992 0.0451,-0.0451 0.05726,-0.29765 0.165365,-0.29765 0.0644,0 0.153656,0.37551 0.23151,0.29765 0.165177,-0.16518 0.04479,-0.35416 0.132292,-0.52916 0.03399,-0.068 0.304612,0.0923 0.330729,0.0661 0.04545,-0.0454 0.0833,-0.51201 0.03307,-0.56224 -0.01558,-0.0156 -0.06615,0.022 -0.06615,0 0,-0.028 0.281985,-0.0505 0.396875,-0.16536 0.170773,-0.17078 -0.246647,-0.43275 -0.23151,-0.46302 0.06965,-0.13931 0.410096,-0.40351 0.297656,-0.62839 -0.04614,-0.0923 -0.252466,-0.0419 -0.297656,-0.13229 -0.0456,-0.0912 0.135223,-0.40567 0.165364,-0.49609 0.06959,-0.20878 -0.199422,-0.10217 -0.23151,-0.19844 -0.04296,-0.12889 0.0105,-0.29918 -0.03307,-0.42995 -0.02453,-0.0736 -0.123415,-0.12785 -0.198438,-0.16536 -0.0093,-0.005 -0.132291,0.004 -0.132291,0 0,-0.0397 0.04837,-0.0637 0.06614,-0.0992 0.03135,-0.0627 0.0317,-0.51469 0,-0.56224 -0.03276,-0.0491 -0.07695,-0.11385 -0.132291,-0.13229 -0.03302,-0.011 -0.187235,0.0224 -0.198438,0 -0.05944,-0.11888 0.01614,-0.33154 -0.03307,-0.42995 -0.0586,-0.1172 -0.266829,-0.007 -0.297656,-0.0992 -0.04371,-0.13112 0.06132,-0.34039 0,-0.46302 -0.02999,-0.06 -0.198437,0.034 -0.198437,-0.0331 0,-0.11173 0.17916,-0.4685 0.09922,-0.62838 -0.04942,-0.0988 -0.363802,-0.0425 -0.363802,-0.0661 0,-0.1322 0.02951,-0.50679 -0.03307,-0.69453 -0.03472,-0.10416 -0.274746,0.0229 -0.330729,-0.0331 -0.0096,-0.01 0.05422,-0.43727 -0.03307,-0.61185 -0.06751,-0.13503 -0.215849,-0.16627 -0.264583,-0.21497 -0.07468,-0.0747 -0.0071,-0.28038 -0.132292,-0.36381 -0.08872,-0.0592 -0.330729,-0.32932 -0.330729,-0.16536 0,0.0136 -0.0088,0.0905 0,0.0992 0.01103,0.011 0.02204,-0.022 0.03307,-0.0331 0.200316,-0.20032 0.238109,-0.31753 0.09922,-0.59532 -0.04997,-0.0999 -0.206341,-0.0654 -0.256315,-0.16532 -0.06174,-0.12348 0.02039,-0.24033 -0.04134,-0.3638 -0.03095,-0.0619 -0.182777,-0.18974 -0.297657,-0.1323 -0.07075,0.0354 -0.09922,0.13824 -0.09922,0.1323 0,-0.0206 0.01685,-0.18159 0,-0.19844 0,0 0.0018,-0.19983 -0.177667,-0.11008 -0.07234,0.0362 -0.139078,0.12534 -0.124127,0.2093 0.02224,0.0834 -0.02386,0.27019 -0.06615,0.18601 0,0 -0.05488,-0.0171 -0.06201,-0.0207 -0.105585,-0.0528 -0.25798,-0.0177 -0.305925,0.0785 -0.117639,0.23528 0.164973,0.31833 0.107487,0.31833 -0.288925,0 -0.311104,-0.0724 -0.44235,0.19017 -0.07048,0.14096 0.107507,0.30592 0.07855,0.30592 -0.658056,0 -0.205991,0.47058 -0.23151,0.49609 -0.10727,0.10727 -0.564973,0.058 -0.463021,0.36381 0.01188,0.0356 0.07119,0.1136 0.09922,0.13229 0.01834,0.0122 0.06614,-0.022 0.06614,0 0,0.0562 -0.115085,0.008 -0.165364,0.0331 -0.08407,0.042 -0.327554,0.19209 -0.363802,0.26458 -0.138208,0.27642 0.264583,0.15169 0.264583,0.33073 0,0.002 -0.193765,-0.002 -0.198437,0 -0.03285,0.0164 -0.191543,0.11851 -0.198438,0.1323 -0.08115,0.1623 0.09922,0.41572 0.09922,0.46302 0,0.0441 -0.08819,0 -0.132292,0 -0.1,0 -0.186081,0.0538 -0.23151,0.0992 -0.01154,0.0115 0.01373,0.0924 0,0.0992 -0.543674,0.27184 0.422574,0.33073 -0.165365,0.33073 -0.257926,0 -0.297656,0.41245 -0.297656,0.62839 0,0.0573 0.132805,0.032 0.09922,0.0992 -0.0578,0.11559 -0.0046,-0.0616 -0.09922,0.0331 -0.01558,0.0156 0.0099,0.0464 0,0.0661 -0.06361,0.12722 -0.233836,0.17233 -0.297656,0.3638 -0.0084,0.0253 -0.02098,0.24359 0,0.26458 0.0075,0.007 0.150582,0.0148 0.09922,0.0661 -0.107569,0.10757 -0.262985,-0.005 -0.330729,0.19843 -0.0534,0.16022 0.256453,0.37194 0.165365,0.46303 -0.0754,0.0754 -0.42972,0.13274 -0.330729,0.33072 0.0081,0.0161 0.145285,0.0791 0.165364,0.0992 0.0865,0.0865 0.0587,0.1323 0.198438,0.1323 0.06615,0 -0.135686,-0.0209 -0.198438,0 -0.09233,0.0308 -0.265856,0.20225 -0.297656,0.29765 -0.01924,0.0577 -0.147865,0.44745 -0.06615,0.52917 0.01818,0.0182 0.306184,0.0907 0.264584,0.13229 -0.01656,0.0166 -0.110609,0.0114 -0.132292,0.0331 -0.146923,0.14692 -0.198438,0.31828 -0.198438,0.59532 0,0.17895 0.282859,0.11749 0.363802,0.19843 0.02455,0.0246 -0.0898,0.10403 -0.09922,0.13229 -0.02432,0.073 -0.07516,0.27964 -0.03307,0.36381 0.0965,0.193 0.501565,0.13411 0.694531,0.19843 0.079,0.0263 0.01249,0.23592 0.03307,0.29766 0.05531,0.16593 0.210219,0.27637 0.297656,0.3638 0.05226,0.0523 0.580541,0.0148 0.628386,-0.0331 0.05937,-0.0594 0.01527,-0.23151 0.09922,-0.23151 0.0065,0 -0.0086,0.21423 0,0.23151 0.05139,0.10277 0.07826,0.16536 0.198438,0.16536 0.08658,0 0.472186,0.057 0.529167,0 0.140168,-0.14016 0.18167,-0.42202 0.330729,-0.19843 0.106537,0.1598 0.47073,0.25301 0.595312,0.0661 0.03845,-0.0577 0.03463,-0.16692 0.06615,-0.19844 0.0059,-0.006 0.124836,0.1451 0.165365,0.16537 0.05867,0.0293 0.280095,-0.0604 0.264583,-0.0992 -0.02048,-0.0512 -0.06615,-0.0882 -0.09922,-0.13229 z"
                            id="path7157"
                        />
                        <path
                            id="path7159"
                            d="m 44.299271,127.67574 c 0.08887,0.0998 0.490573,0.45574 0.610964,0.1864 0.02404,-0.0538 0.0097,-0.13514 0.05555,-0.1864 0.04838,-0.0541 0.152391,0.1629 0.194399,0.1864 0.01848,0.0103 0.273089,0.0103 0.27771,0 0.0283,-0.0633 -0.02139,-0.20549 0.05555,-0.24853 0.117422,-0.0657 0.383298,0.22361 0.499879,0.0932 0.03787,-0.0424 0.04808,-0.27959 0.138855,-0.27959 0.05407,0 0.129025,0.35272 0.194398,0.27959 0.138699,-0.15516 0.03761,-0.33267 0.111085,-0.49704 0.02854,-0.0639 0.255781,0.0867 0.277711,0.0621 0.03816,-0.0426 0.06995,-0.48093 0.02777,-0.52811 -0.01309,-0.0147 -0.05555,0.0207 -0.05555,0 0,-0.0263 0.23678,-0.0474 0.333253,-0.15532 0.143397,-0.16042 -0.207108,-0.40649 -0.194398,-0.43492 0.05848,-0.13085 0.344356,-0.37902 0.24994,-0.59025 -0.03874,-0.0867 -0.211993,-0.0393 -0.24994,-0.12426 -0.03829,-0.0857 0.113546,-0.38104 0.138855,-0.46597 0.05844,-0.19611 -0.167452,-0.096 -0.194397,-0.1864 -0.03608,-0.12107 0.0088,-0.28102 -0.02777,-0.40385 -0.0206,-0.0691 -0.103631,-0.12009 -0.166628,-0.15533 -0.0078,-0.005 -0.111083,0.004 -0.111083,0 0,-0.0373 0.04062,-0.0598 0.05554,-0.0932 0.02632,-0.0589 0.02661,-0.48345 0,-0.52812 -0.02751,-0.0461 -0.06462,-0.10694 -0.111084,-0.12426 -0.02773,-0.0103 -0.15722,0.021 -0.166626,0 -0.04991,-0.11166 0.01355,-0.31141 -0.02777,-0.40385 -0.04921,-0.11009 -0.224054,-0.007 -0.24994,-0.0932 -0.0367,-0.12316 0.05149,-0.31973 0,-0.43492 -0.02519,-0.0563 -0.166625,0.0319 -0.166625,-0.0311 0,-0.10495 0.150439,-0.44006 0.08331,-0.59024 -0.04149,-0.0928 -0.305483,-0.0399 -0.305483,-0.0621 0,-0.12418 0.02478,-0.47603 -0.02777,-0.65238 -0.02916,-0.0978 -0.230702,0.0215 -0.27771,-0.0311 -0.0081,-0.009 0.04553,-0.41073 -0.02777,-0.57471 -0.05669,-0.12683 -0.181247,-0.15618 -0.222168,-0.20192 -0.06271,-0.0702 -0.006,-0.26336 -0.111085,-0.34173 -0.0745,-0.0556 -0.277711,-0.30933 -0.277711,-0.15532 0,0.0128 -0.0074,0.085 0,0.0932 0.0093,0.0103 0.0185,-0.0207 0.02777,-0.0311 0.168203,-0.18817 0.199938,-0.29826 0.08332,-0.55919 -0.04196,-0.0938 -0.173264,-0.0614 -0.215227,-0.15529 -0.05185,-0.11598 0.01712,-0.22574 -0.03471,-0.34171 -0.02598,-0.0581 -0.153477,-0.17823 -0.249941,-0.12427 -0.0594,0.0333 -0.08331,0.12985 -0.08331,0.12427 0,-0.0194 0.01415,-0.17057 0,-0.1864 0,0 0.0015,-0.1877 -0.149186,-0.1034 -0.06075,0.034 -0.116783,0.11774 -0.104229,0.1966 0.01867,0.0783 -0.02003,0.25379 -0.05555,0.17472 0,0 -0.04608,-0.0161 -0.05207,-0.0194 -0.08866,-0.0496 -0.216624,-0.0166 -0.256883,0.0737 -0.09878,0.221 0.138526,0.29901 0.09025,0.29901 -0.242609,0 -0.261232,-0.068 -0.371439,0.17863 -0.05918,0.1324 0.09027,0.28735 0.06596,0.28735 -0.552566,0 -0.17297,0.44201 -0.194398,0.46598 -0.09007,0.10075 -0.474404,0.0545 -0.388795,0.34172 0.01,0.0334 0.05978,0.10671 0.08331,0.12426 0.0154,0.0115 0.05554,-0.0207 0.05554,0 0,0.0528 -0.09664,0.008 -0.138855,0.0311 -0.07059,0.0395 -0.275045,0.18043 -0.305482,0.24852 -0.116052,0.25965 0.222169,0.14249 0.222169,0.31066 0,0.002 -0.162703,-0.002 -0.166627,0 -0.02758,0.0154 -0.160837,0.11132 -0.166627,0.12427 -0.06815,0.15245 0.08331,0.39049 0.08331,0.43492 0,0.0414 -0.07405,0 -0.111085,0 -0.08397,0 -0.156251,0.0505 -0.194397,0.0932 -0.0097,0.0108 0.01153,0.0868 0,0.0932 -0.456519,0.25534 0.354832,0.31066 -0.138856,0.31066 -0.216579,0 -0.24994,0.38741 -0.24994,0.59025 0,0.0538 0.111515,0.0301 0.08331,0.0932 -0.04853,0.10857 -0.0038,-0.0579 -0.08331,0.0311 -0.01309,0.0147 0.0084,0.0436 0,0.0621 -0.05342,0.1195 -0.196351,0.16187 -0.24994,0.34172 -0.007,0.0238 -0.01762,0.22881 0,0.24852 0.0063,0.007 0.126443,0.0139 0.08331,0.0621 -0.09033,0.10104 -0.220826,-0.005 -0.27771,0.18639 -0.04483,0.15049 0.215342,0.34936 0.138855,0.43492 -0.06331,0.0708 -0.360833,0.12468 -0.27771,0.31065 0.0068,0.0151 0.121995,0.0743 0.138855,0.0932 0.07263,0.0812 0.04929,0.12427 0.166627,0.12427 0.05555,0 -0.113935,-0.0196 -0.166627,0 -0.07753,0.0289 -0.223238,0.18997 -0.24994,0.27958 -0.01616,0.0542 -0.124161,0.42029 -0.05555,0.49705 0.01526,0.0171 0.2571,0.0852 0.222169,0.12426 -0.0139,0.0156 -0.09288,0.0107 -0.111085,0.0311 -0.12337,0.138 -0.166627,0.29896 -0.166627,0.55919 0,0.16808 0.237515,0.11036 0.305483,0.18638 0.02061,0.0231 -0.0754,0.0977 -0.08332,0.12426 -0.02042,0.0686 -0.06311,0.26267 -0.02777,0.34173 0.08103,0.18129 0.42116,0.12597 0.583193,0.18639 0.06633,0.0247 0.01049,0.2216 0.02777,0.27959 0.04644,0.15586 0.176519,0.25959 0.24994,0.34172 0.04389,0.0491 0.487475,0.0139 0.527651,-0.0311 0.04986,-0.0558 0.01282,-0.21746 0.08331,-0.21746 0.0054,0 -0.0072,0.20122 0,0.21746 0.04315,0.0965 0.06571,0.15532 0.166627,0.15532 0.0727,0 0.396492,0.0535 0.444338,0 0.117698,-0.13165 0.152548,-0.39641 0.277711,-0.18639 0.08946,0.1501 0.395268,0.23766 0.499879,0.0621 0.03228,-0.0542 0.02908,-0.15679 0.05555,-0.18639 0.005,-0.006 0.104824,0.13629 0.138856,0.15533 0.04927,0.0275 0.235194,-0.0567 0.222168,-0.0932 -0.0172,-0.0481 -0.05555,-0.0829 -0.08331,-0.12426 z"
                            style="fill:#497c2f;fill-opacity:1;stroke:none;stroke-width:0.23497705px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                        />
                        </g>
                    </g>
                    <g
                        style="display:inline"
                        id="g7185"
                        transform="translate(14.592916,-6.7351921)"
                    >
                        <g id="g7177">
                        <path
                            d="m 136.47461,491.33789 c -0.98152,0 -1.77149,0.78997 -1.77149,1.77149 v 36.32031 c 0,0.32623 0.0989,0.62488 0.25196,0.88672 0.0244,-0.0205 0.0482,-0.0419 0.0723,-0.0625 -0.0113,0.004 -0.0422,0.0236 -0.0332,0.0156 0.0867,-0.0733 0.16743,-0.15424 0.26563,-0.21094 0.10801,-0.0626 0.24118,-0.0929 0.37695,-0.10547 0.13577,-0.0124 0.27438,-0.008 0.39453,0.006 0.1245,0.034 0.25531,-0.003 0.38086,0.0117 0.15224,0.0189 0.29311,0.0603 0.43945,0.0996 0.13285,0.008 0.25211,0.0767 0.38086,0.10352 0.0136,0.003 0.043,-0.0155 0.0625,-0.0195 0.0333,-0.004 0.0662,-0.006 0.0996,-0.01 0.18093,-0.0119 0.27869,0.0221 0.39258,0.084 0.0612,-7.7e-4 0.12243,-0.002 0.18359,-0.002 0.17809,0.002 0.35511,1.9e-4 0.53321,0 0.0234,1e-5 0.0469,0 0.0703,0 0.12122,-0.24015 0.19531,-0.50851 0.19531,-0.79687 v -36.32031 c 0,-0.98152 -0.78996,-1.77149 -1.77148,-1.77149 z"
                            style="display:inline;opacity:1;vector-effect:none;fill:#794327;fill-opacity:1;stroke:none;stroke-width:1.60390437;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            transform="scale(0.26458333)"
                            id="path7173"
                        />
                        <path
                            d="m 136.0293,491.33789 c -0.73451,0 -1.32618,0.78997 -1.32618,1.77149 v 36.32031 c 0,0.25334 0.0422,0.49339 0.11329,0.71093 h 0.31836 c 0.0663,0 0.13341,-0.008 0.19921,0 0.0967,0.0113 0.18966,0.0377 0.28516,0.0566 0.0254,0.006 0.051,0.0125 0.0762,0.0195 0.0276,-0.005 0.055,-0.005 0.0859,-0.01 0.25123,-0.0378 0.46664,-0.0199 0.66797,0.15625 -0.0254,-0.0113 -0.10097,-0.0425 -0.0762,-0.0293 0.0527,0.0265 0.10815,0.0451 0.16015,0.0723 0.0242,0.0114 0.0624,0.0197 0.0664,0.0469 0.003,0.0185 -0.0769,0.0215 -0.0566,0.0215 0.0575,0 0.11487,-0.0135 0.17187,-0.0215 0.0726,0.004 0.1441,0.008 0.2168,0.0117 0.0485,0.014 0.0975,0.0263 0.14648,0.0391 0.17113,-0.29838 0.27735,-0.66758 0.27735,-1.07422 v -36.32031 c 0,-0.98152 -0.59167,-1.77149 -1.32617,-1.77149 z"
                            style="display:inline;opacity:1;vector-effect:none;fill:url(#linearGradient7187);fill-opacity:1;stroke:none;stroke-width:1.29526961;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            transform="scale(0.26458333)"
                            id="path7175"
                        />
                        </g>
                        <g id="g7183">
                        <path
                            id="path7179"
                            d="m 33.023307,129.79987 c 0.165809,0.98597 0.785387,0.3263 1.422622,0.64492 0.637235,0.31862 0.705217,0.28291 1.157066,0.1323 0.451849,-0.15062 0.276133,-0.33074 0.843359,-0.33074 0.567226,0 0.726157,0.39132 1.352951,0.18239 0.626794,-0.20893 0.45413,-0.13819 1.16059,-0.57926 0.70646,-0.44107 1.052781,-0.96643 1.372527,-1.71979 0.319746,-0.75336 0.693698,-0.95426 0.464825,-2.0083 -0.228873,-1.05405 -0.11062,-1.37489 -0.712872,-1.87777 -0.602252,-0.50288 -0.446893,-0.98285 -0.856917,-1.39287 -0.410023,-0.41002 -0.799047,-0.32699 -1.308671,-0.70256 -0.509623,-0.37556 -1.391401,-0.55473 -2.157278,-0.48945 -0.765877,0.0653 -1.482282,0.43279 -2.035164,0.96881 -0.552882,0.53602 -0.680309,0.10105 -1.452857,0.87359 -0.772548,0.77253 -0.323878,0.99008 -0.523492,1.85042 -0.199614,0.86034 -0.714776,1.09126 -0.33073,1.78594 0.384046,0.69468 -0.03042,0.84563 0.479558,1.48828 0.509976,0.64264 0.958674,0.18811 1.124483,1.17409 z"
                            style="fill:#5d973c;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                        />
                        <path
                            style="fill:#487b2e;fill-opacity:1;stroke:none;stroke-width:0.24757226px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                            d="m 32.901261,129.32272 c 0.154196,0.92827 0.730381,0.30721 1.322986,0.60718 0.592605,0.29998 0.655826,0.26636 1.076029,0.12456 0.420203,-0.1418 0.256793,-0.31139 0.784293,-0.31139 0.527499,0 0.675299,0.36842 1.258194,0.17172 0.582895,-0.1967 0.422324,-0.1301 1.079306,-0.54536 0.656981,-0.41526 0.979048,-0.90989 1.276399,-1.61917 0.297352,-0.70927 0.645114,-0.89841 0.43227,-1.89078 -0.212843,-0.99237 -0.102876,-1.29443 -0.662944,-1.76789 -0.560073,-0.47345 -0.415595,-0.92533 -0.796901,-1.31136 -0.381307,-0.38603 -0.743084,-0.30786 -1.217016,-0.66145 -0.47393,-0.35359 -1.293951,-0.52227 -2.006189,-0.46081 -0.712238,0.0615 -1.378467,0.40747 -1.892628,0.91212 -0.51416,0.50465 -0.632661,0.0951 -1.351102,0.82246 -0.718441,0.72734 -0.301195,0.93216 -0.486829,1.74215 -0.185634,0.81 -0.664715,1.0274 -0.307567,1.68143 0.357149,0.65404 -0.02829,0.79616 0.445972,1.4012 0.474259,0.60504 0.891531,0.17711 1.045727,1.10539 z"
                            id="path7181"
                        />
                        </g>
                    </g>
                    <g id="g7201" transform="translate(64.171413,-23.198995)">
                        <g
                        id="g7193"
                        transform="matrix(0.50816809,0,0,0.75105971,26.326383,30.60931)"
                        style="display:inline"
                        >
                        <path
                            id="path7189"
                            transform="scale(0.26458333)"
                            style="display:inline;opacity:1;vector-effect:none;fill:#794327;fill-opacity:1;stroke:none;stroke-width:1.60390437;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 136.47461,491.33789 c -0.98152,0 -1.77149,0.78997 -1.77149,1.77149 v 36.32031 c 0,0.32623 0.0989,0.62488 0.25196,0.88672 0.0244,-0.0205 0.0482,-0.0419 0.0723,-0.0625 -0.0113,0.004 -0.0422,0.0236 -0.0332,0.0156 0.0867,-0.0733 0.16743,-0.15424 0.26563,-0.21094 0.10801,-0.0626 0.24118,-0.0929 0.37695,-0.10547 0.13577,-0.0124 0.27438,-0.008 0.39453,0.006 0.1245,0.034 0.25531,-0.003 0.38086,0.0117 0.15224,0.0189 0.29311,0.0603 0.43945,0.0996 0.13285,0.008 0.25211,0.0767 0.38086,0.10352 0.0136,0.003 0.043,-0.0155 0.0625,-0.0195 0.0333,-0.004 0.0662,-0.006 0.0996,-0.01 0.18093,-0.0119 0.27869,0.0221 0.39258,0.084 0.0612,-7.7e-4 0.12243,-0.002 0.18359,-0.002 0.17809,0.002 0.35511,1.9e-4 0.53321,0 0.0234,1e-5 0.0469,0 0.0703,0 0.12122,-0.24015 0.19531,-0.50851 0.19531,-0.79687 v -36.32031 c 0,-0.98152 -0.78996,-1.77149 -1.77148,-1.77149 z"
                        />
                        <path
                            id="path7191"
                            transform="scale(0.26458333)"
                            style="display:inline;opacity:1;vector-effect:none;fill:url(#linearGradient7203);fill-opacity:1;stroke:none;stroke-width:1.29526961;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            d="m 136.0293,491.33789 c -0.73451,0 -1.32618,0.78997 -1.32618,1.77149 v 36.32031 c 0,0.25334 0.0422,0.49339 0.11329,0.71093 h 0.31836 c 0.0663,0 0.13341,-0.008 0.19921,0 0.0967,0.0113 0.18966,0.0377 0.28516,0.0566 0.0254,0.006 0.051,0.0125 0.0762,0.0195 0.0276,-0.005 0.055,-0.005 0.0859,-0.01 0.25123,-0.0378 0.46664,-0.0199 0.66797,0.15625 -0.0254,-0.0113 -0.10097,-0.0425 -0.0762,-0.0293 0.0527,0.0265 0.10815,0.0451 0.16015,0.0723 0.0242,0.0114 0.0624,0.0197 0.0664,0.0469 0.003,0.0185 -0.0769,0.0215 -0.0566,0.0215 0.0575,0 0.11487,-0.0135 0.17187,-0.0215 0.0726,0.004 0.1441,0.008 0.2168,0.0117 0.0485,0.014 0.0975,0.0263 0.14648,0.0391 0.17113,-0.29838 0.27735,-0.66758 0.27735,-1.07422 v -36.32031 c 0,-0.98152 -0.59167,-1.77149 -1.32617,-1.77149 z"
                        />
                        </g>
                        <g id="g7199">
                        <path
                            id="path7195"
                            d="m 44.946093,128.26198 c 0.105839,0.10626 0.584229,0.48519 0.727604,0.19844 0.02863,-0.0573 0.01159,-0.14387 0.06615,-0.19844 0.05761,-0.0576 0.181483,0.17342 0.231511,0.19844 0.02201,0.011 0.325225,0.011 0.330729,0 0.03371,-0.0674 -0.02548,-0.21877 0.06615,-0.26459 0.13984,-0.0699 0.456475,0.23806 0.595312,0.0992 0.0451,-0.0451 0.05726,-0.29765 0.165365,-0.29765 0.0644,0 0.153656,0.37551 0.23151,0.29765 0.165177,-0.16518 0.04479,-0.35416 0.132292,-0.52916 0.03399,-0.068 0.304612,0.0923 0.330729,0.0661 0.04545,-0.0454 0.0833,-0.51201 0.03307,-0.56224 -0.01558,-0.0156 -0.06615,0.022 -0.06615,0 0,-0.028 0.281985,-0.0505 0.396875,-0.16536 0.170773,-0.17078 -0.246647,-0.43275 -0.23151,-0.46302 0.06965,-0.13931 0.410096,-0.40351 0.297656,-0.62839 -0.04614,-0.0923 -0.252466,-0.0419 -0.297656,-0.13229 -0.0456,-0.0912 0.135223,-0.40567 0.165364,-0.49609 0.06959,-0.20878 -0.199422,-0.10217 -0.23151,-0.19844 -0.04296,-0.12889 0.0105,-0.29918 -0.03307,-0.42995 -0.02453,-0.0736 -0.123415,-0.12785 -0.198438,-0.16536 -0.0093,-0.005 -0.132291,0.004 -0.132291,0 0,-0.0397 0.04837,-0.0637 0.06614,-0.0992 0.03135,-0.0627 0.0317,-0.51469 0,-0.56224 -0.03276,-0.0491 -0.07695,-0.11385 -0.132291,-0.13229 -0.03302,-0.011 -0.187235,0.0224 -0.198438,0 -0.05944,-0.11888 0.01614,-0.33154 -0.03307,-0.42995 -0.0586,-0.1172 -0.266829,-0.007 -0.297656,-0.0992 -0.04371,-0.13112 0.06132,-0.34039 0,-0.46302 -0.02999,-0.06 -0.198437,0.034 -0.198437,-0.0331 0,-0.11173 0.17916,-0.4685 0.09922,-0.62838 -0.04942,-0.0988 -0.363802,-0.0425 -0.363802,-0.0661 0,-0.1322 0.02951,-0.50679 -0.03307,-0.69453 -0.03472,-0.10416 -0.274746,0.0229 -0.330729,-0.0331 -0.0096,-0.01 0.05422,-0.43727 -0.03307,-0.61185 -0.06751,-0.13503 -0.215849,-0.16627 -0.264583,-0.21497 -0.07468,-0.0747 -0.0071,-0.28038 -0.132292,-0.36381 -0.08872,-0.0592 -0.330729,-0.32932 -0.330729,-0.16536 0,0.0136 -0.0088,0.0905 0,0.0992 0.01103,0.011 0.02204,-0.022 0.03307,-0.0331 0.200316,-0.20032 0.238109,-0.31753 0.09922,-0.59532 -0.04997,-0.0999 -0.206341,-0.0654 -0.256315,-0.16532 -0.06174,-0.12348 0.02039,-0.24033 -0.04134,-0.3638 -0.03095,-0.0619 -0.182777,-0.18974 -0.297657,-0.1323 -0.07075,0.0354 -0.09922,0.13824 -0.09922,0.1323 0,-0.0206 0.01685,-0.18159 0,-0.19844 0,0 0.0018,-0.19983 -0.177667,-0.11008 -0.07234,0.0362 -0.139078,0.12534 -0.124127,0.2093 0.02224,0.0834 -0.02386,0.27019 -0.06615,0.18601 0,0 -0.05488,-0.0171 -0.06201,-0.0207 -0.105585,-0.0528 -0.25798,-0.0177 -0.305925,0.0785 -0.117639,0.23528 0.164973,0.31833 0.107487,0.31833 -0.288925,0 -0.311104,-0.0724 -0.44235,0.19017 -0.07048,0.14096 0.107507,0.30592 0.07855,0.30592 -0.658056,0 -0.205991,0.47058 -0.23151,0.49609 -0.10727,0.10727 -0.564973,0.058 -0.463021,0.36381 0.01188,0.0356 0.07119,0.1136 0.09922,0.13229 0.01834,0.0122 0.06614,-0.022 0.06614,0 0,0.0562 -0.115085,0.008 -0.165364,0.0331 -0.08407,0.042 -0.327554,0.19209 -0.363802,0.26458 -0.138208,0.27642 0.264583,0.15169 0.264583,0.33073 0,0.002 -0.193765,-0.002 -0.198437,0 -0.03285,0.0164 -0.191543,0.11851 -0.198438,0.1323 -0.08115,0.1623 0.09922,0.41572 0.09922,0.46302 0,0.0441 -0.08819,0 -0.132292,0 -0.1,0 -0.186081,0.0538 -0.23151,0.0992 -0.01154,0.0115 0.01373,0.0924 0,0.0992 -0.543674,0.27184 0.422574,0.33073 -0.165365,0.33073 -0.257926,0 -0.297656,0.41245 -0.297656,0.62839 0,0.0573 0.132805,0.032 0.09922,0.0992 -0.0578,0.11559 -0.0046,-0.0616 -0.09922,0.0331 -0.01558,0.0156 0.0099,0.0464 0,0.0661 -0.06361,0.12722 -0.233836,0.17233 -0.297656,0.3638 -0.0084,0.0253 -0.02098,0.24359 0,0.26458 0.0075,0.007 0.150582,0.0148 0.09922,0.0661 -0.107569,0.10757 -0.262985,-0.005 -0.330729,0.19843 -0.0534,0.16022 0.256453,0.37194 0.165365,0.46303 -0.0754,0.0754 -0.42972,0.13274 -0.330729,0.33072 0.0081,0.0161 0.145285,0.0791 0.165364,0.0992 0.0865,0.0865 0.0587,0.1323 0.198438,0.1323 0.06615,0 -0.135686,-0.0209 -0.198438,0 -0.09233,0.0308 -0.265856,0.20225 -0.297656,0.29765 -0.01924,0.0577 -0.147865,0.44745 -0.06615,0.52917 0.01818,0.0182 0.306184,0.0907 0.264584,0.13229 -0.01656,0.0166 -0.110609,0.0114 -0.132292,0.0331 -0.146923,0.14692 -0.198438,0.31828 -0.198438,0.59532 0,0.17895 0.282859,0.11749 0.363802,0.19843 0.02455,0.0246 -0.0898,0.10403 -0.09922,0.13229 -0.02432,0.073 -0.07516,0.27964 -0.03307,0.36381 0.0965,0.193 0.501565,0.13411 0.694531,0.19843 0.079,0.0263 0.01249,0.23592 0.03307,0.29766 0.05531,0.16593 0.210219,0.27637 0.297656,0.3638 0.05226,0.0523 0.580541,0.0148 0.628386,-0.0331 0.05937,-0.0594 0.01527,-0.23151 0.09922,-0.23151 0.0065,0 -0.0086,0.21423 0,0.23151 0.05139,0.10277 0.07826,0.16536 0.198438,0.16536 0.08658,0 0.472186,0.057 0.529167,0 0.140168,-0.14016 0.18167,-0.42202 0.330729,-0.19843 0.106537,0.1598 0.47073,0.25301 0.595312,0.0661 0.03845,-0.0577 0.03463,-0.16692 0.06615,-0.19844 0.0059,-0.006 0.124836,0.1451 0.165365,0.16537 0.05867,0.0293 0.280095,-0.0604 0.264583,-0.0992 -0.02048,-0.0512 -0.06615,-0.0882 -0.09922,-0.13229 z"
                            style="fill:#5a963a;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                        />
                        <path
                            style="fill:#497c2f;fill-opacity:1;stroke:none;stroke-width:0.23497705px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                            d="m 44.299271,127.67574 c 0.08887,0.0998 0.490573,0.45574 0.610964,0.1864 0.02404,-0.0538 0.0097,-0.13514 0.05555,-0.1864 0.04838,-0.0541 0.152391,0.1629 0.194399,0.1864 0.01848,0.0103 0.273089,0.0103 0.27771,0 0.0283,-0.0633 -0.02139,-0.20549 0.05555,-0.24853 0.117422,-0.0657 0.383298,0.22361 0.499879,0.0932 0.03787,-0.0424 0.04808,-0.27959 0.138855,-0.27959 0.05407,0 0.129025,0.35272 0.194398,0.27959 0.138699,-0.15516 0.03761,-0.33267 0.111085,-0.49704 0.02854,-0.0639 0.255781,0.0867 0.277711,0.0621 0.03816,-0.0426 0.06995,-0.48093 0.02777,-0.52811 -0.01309,-0.0147 -0.05555,0.0207 -0.05555,0 0,-0.0263 0.23678,-0.0474 0.333253,-0.15532 0.143397,-0.16042 -0.207108,-0.40649 -0.194398,-0.43492 0.05848,-0.13085 0.344356,-0.37902 0.24994,-0.59025 -0.03874,-0.0867 -0.211993,-0.0393 -0.24994,-0.12426 -0.03829,-0.0857 0.113546,-0.38104 0.138855,-0.46597 0.05844,-0.19611 -0.167452,-0.096 -0.194397,-0.1864 -0.03608,-0.12107 0.0088,-0.28102 -0.02777,-0.40385 -0.0206,-0.0691 -0.103631,-0.12009 -0.166628,-0.15533 -0.0078,-0.005 -0.111083,0.004 -0.111083,0 0,-0.0373 0.04062,-0.0598 0.05554,-0.0932 0.02632,-0.0589 0.02661,-0.48345 0,-0.52812 -0.02751,-0.0461 -0.06462,-0.10694 -0.111084,-0.12426 -0.02773,-0.0103 -0.15722,0.021 -0.166626,0 -0.04991,-0.11166 0.01355,-0.31141 -0.02777,-0.40385 -0.04921,-0.11009 -0.224054,-0.007 -0.24994,-0.0932 -0.0367,-0.12316 0.05149,-0.31973 0,-0.43492 -0.02519,-0.0563 -0.166625,0.0319 -0.166625,-0.0311 0,-0.10495 0.150439,-0.44006 0.08331,-0.59024 -0.04149,-0.0928 -0.305483,-0.0399 -0.305483,-0.0621 0,-0.12418 0.02478,-0.47603 -0.02777,-0.65238 -0.02916,-0.0978 -0.230702,0.0215 -0.27771,-0.0311 -0.0081,-0.009 0.04553,-0.41073 -0.02777,-0.57471 -0.05669,-0.12683 -0.181247,-0.15618 -0.222168,-0.20192 -0.06271,-0.0702 -0.006,-0.26336 -0.111085,-0.34173 -0.0745,-0.0556 -0.277711,-0.30933 -0.277711,-0.15532 0,0.0128 -0.0074,0.085 0,0.0932 0.0093,0.0103 0.0185,-0.0207 0.02777,-0.0311 0.168203,-0.18817 0.199938,-0.29826 0.08332,-0.55919 -0.04196,-0.0938 -0.173264,-0.0614 -0.215227,-0.15529 -0.05185,-0.11598 0.01712,-0.22574 -0.03471,-0.34171 -0.02598,-0.0581 -0.153477,-0.17823 -0.249941,-0.12427 -0.0594,0.0333 -0.08331,0.12985 -0.08331,0.12427 0,-0.0194 0.01415,-0.17057 0,-0.1864 0,0 0.0015,-0.1877 -0.149186,-0.1034 -0.06075,0.034 -0.116783,0.11774 -0.104229,0.1966 0.01867,0.0783 -0.02003,0.25379 -0.05555,0.17472 0,0 -0.04608,-0.0161 -0.05207,-0.0194 -0.08866,-0.0496 -0.216624,-0.0166 -0.256883,0.0737 -0.09878,0.221 0.138526,0.29901 0.09025,0.29901 -0.242609,0 -0.261232,-0.068 -0.371439,0.17863 -0.05918,0.1324 0.09027,0.28735 0.06596,0.28735 -0.552566,0 -0.17297,0.44201 -0.194398,0.46598 -0.09007,0.10075 -0.474404,0.0545 -0.388795,0.34172 0.01,0.0334 0.05978,0.10671 0.08331,0.12426 0.0154,0.0115 0.05554,-0.0207 0.05554,0 0,0.0528 -0.09664,0.008 -0.138855,0.0311 -0.07059,0.0395 -0.275045,0.18043 -0.305482,0.24852 -0.116052,0.25965 0.222169,0.14249 0.222169,0.31066 0,0.002 -0.162703,-0.002 -0.166627,0 -0.02758,0.0154 -0.160837,0.11132 -0.166627,0.12427 -0.06815,0.15245 0.08331,0.39049 0.08331,0.43492 0,0.0414 -0.07405,0 -0.111085,0 -0.08397,0 -0.156251,0.0505 -0.194397,0.0932 -0.0097,0.0108 0.01153,0.0868 0,0.0932 -0.456519,0.25534 0.354832,0.31066 -0.138856,0.31066 -0.216579,0 -0.24994,0.38741 -0.24994,0.59025 0,0.0538 0.111515,0.0301 0.08331,0.0932 -0.04853,0.10857 -0.0038,-0.0579 -0.08331,0.0311 -0.01309,0.0147 0.0084,0.0436 0,0.0621 -0.05342,0.1195 -0.196351,0.16187 -0.24994,0.34172 -0.007,0.0238 -0.01762,0.22881 0,0.24852 0.0063,0.007 0.126443,0.0139 0.08331,0.0621 -0.09033,0.10104 -0.220826,-0.005 -0.27771,0.18639 -0.04483,0.15049 0.215342,0.34936 0.138855,0.43492 -0.06331,0.0708 -0.360833,0.12468 -0.27771,0.31065 0.0068,0.0151 0.121995,0.0743 0.138855,0.0932 0.07263,0.0812 0.04929,0.12427 0.166627,0.12427 0.05555,0 -0.113935,-0.0196 -0.166627,0 -0.07753,0.0289 -0.223238,0.18997 -0.24994,0.27958 -0.01616,0.0542 -0.124161,0.42029 -0.05555,0.49705 0.01526,0.0171 0.2571,0.0852 0.222169,0.12426 -0.0139,0.0156 -0.09288,0.0107 -0.111085,0.0311 -0.12337,0.138 -0.166627,0.29896 -0.166627,0.55919 0,0.16808 0.237515,0.11036 0.305483,0.18638 0.02061,0.0231 -0.0754,0.0977 -0.08332,0.12426 -0.02042,0.0686 -0.06311,0.26267 -0.02777,0.34173 0.08103,0.18129 0.42116,0.12597 0.583193,0.18639 0.06633,0.0247 0.01049,0.2216 0.02777,0.27959 0.04644,0.15586 0.176519,0.25959 0.24994,0.34172 0.04389,0.0491 0.487475,0.0139 0.527651,-0.0311 0.04986,-0.0558 0.01282,-0.21746 0.08331,-0.21746 0.0054,0 -0.0072,0.20122 0,0.21746 0.04315,0.0965 0.06571,0.15532 0.166627,0.15532 0.0727,0 0.396492,0.0535 0.444338,0 0.117698,-0.13165 0.152548,-0.39641 0.277711,-0.18639 0.08946,0.1501 0.395268,0.23766 0.499879,0.0621 0.03228,-0.0542 0.02908,-0.15679 0.05555,-0.18639 0.005,-0.006 0.104824,0.13629 0.138856,0.15533 0.04927,0.0275 0.235194,-0.0567 0.222168,-0.0932 -0.0172,-0.0481 -0.05555,-0.0829 -0.08331,-0.12426 z"
                            id="path7197"
                        />
                        </g>
                    </g>
                    <g
                        style="display:inline"
                        id="g7233"
                        transform="matrix(0.86201057,0,0,1,85.804592,-1.3118328)"
                    >
                        <g id="g7225">
                        <path
                            d="m 136.47461,491.33789 c -0.98152,0 -1.77149,0.78997 -1.77149,1.77149 v 36.32031 c 0,0.32623 0.0989,0.62488 0.25196,0.88672 0.0244,-0.0205 0.0482,-0.0419 0.0723,-0.0625 -0.0113,0.004 -0.0422,0.0236 -0.0332,0.0156 0.0867,-0.0733 0.16743,-0.15424 0.26563,-0.21094 0.10801,-0.0626 0.24118,-0.0929 0.37695,-0.10547 0.13577,-0.0124 0.27438,-0.008 0.39453,0.006 0.1245,0.034 0.25531,-0.003 0.38086,0.0117 0.15224,0.0189 0.29311,0.0603 0.43945,0.0996 0.13285,0.008 0.25211,0.0767 0.38086,0.10352 0.0136,0.003 0.043,-0.0155 0.0625,-0.0195 0.0333,-0.004 0.0662,-0.006 0.0996,-0.01 0.18093,-0.0119 0.27869,0.0221 0.39258,0.084 0.0612,-7.7e-4 0.12243,-0.002 0.18359,-0.002 0.17809,0.002 0.35511,1.9e-4 0.53321,0 0.0234,1e-5 0.0469,0 0.0703,0 0.12122,-0.24015 0.19531,-0.50851 0.19531,-0.79687 v -36.32031 c 0,-0.98152 -0.78996,-1.77149 -1.77148,-1.77149 z"
                            style="display:inline;opacity:1;vector-effect:none;fill:#794327;fill-opacity:1;stroke:none;stroke-width:1.60390437;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            transform="scale(0.26458333)"
                            id="path7221"
                        />
                        <path
                            d="m 136.0293,491.33789 c -0.73451,0 -1.32618,0.78997 -1.32618,1.77149 v 36.32031 c 0,0.25334 0.0422,0.49339 0.11329,0.71093 h 0.31836 c 0.0663,0 0.13341,-0.008 0.19921,0 0.0967,0.0113 0.18966,0.0377 0.28516,0.0566 0.0254,0.006 0.051,0.0125 0.0762,0.0195 0.0276,-0.005 0.055,-0.005 0.0859,-0.01 0.25123,-0.0378 0.46664,-0.0199 0.66797,0.15625 -0.0254,-0.0113 -0.10097,-0.0425 -0.0762,-0.0293 0.0527,0.0265 0.10815,0.0451 0.16015,0.0723 0.0242,0.0114 0.0624,0.0197 0.0664,0.0469 0.003,0.0185 -0.0769,0.0215 -0.0566,0.0215 0.0575,0 0.11487,-0.0135 0.17187,-0.0215 0.0726,0.004 0.1441,0.008 0.2168,0.0117 0.0485,0.014 0.0975,0.0263 0.14648,0.0391 0.17113,-0.29838 0.27735,-0.66758 0.27735,-1.07422 v -36.32031 c 0,-0.98152 -0.59167,-1.77149 -1.32617,-1.77149 z"
                            style="display:inline;opacity:1;vector-effect:none;fill:url(#linearGradient7235);fill-opacity:1;stroke:none;stroke-width:1.29526961;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:0;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1;paint-order:stroke fill markers"
                            transform="scale(0.26458333)"
                            id="path7223"
                        />
                        </g>
                        <g id="g7231">
                        <path
                            id="path7227"
                            d="m 33.023307,129.79987 c 0.165809,0.98597 0.785387,0.3263 1.422622,0.64492 0.637235,0.31862 0.705217,0.28291 1.157066,0.1323 0.451849,-0.15062 0.276133,-0.33074 0.843359,-0.33074 0.567226,0 0.726157,0.39132 1.352951,0.18239 0.626794,-0.20893 0.45413,-0.13819 1.16059,-0.57926 0.70646,-0.44107 1.052781,-0.96643 1.372527,-1.71979 0.319746,-0.75336 0.693698,-0.95426 0.464825,-2.0083 -0.228873,-1.05405 -0.11062,-1.37489 -0.712872,-1.87777 -0.602252,-0.50288 -0.446893,-0.98285 -0.856917,-1.39287 -0.410023,-0.41002 -0.799047,-0.32699 -1.308671,-0.70256 -0.509623,-0.37556 -1.391401,-0.55473 -2.157278,-0.48945 -0.765877,0.0653 -1.482282,0.43279 -2.035164,0.96881 -0.552882,0.53602 -0.680309,0.10105 -1.452857,0.87359 -0.772548,0.77253 -0.323878,0.99008 -0.523492,1.85042 -0.199614,0.86034 -0.714776,1.09126 -0.33073,1.78594 0.384046,0.69468 -0.03042,0.84563 0.479558,1.48828 0.509976,0.64264 0.958674,0.18811 1.124483,1.17409 z"
                            style="fill:#5d973c;fill-opacity:1;stroke:none;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                        />
                        <path
                            style="fill:#487b2e;fill-opacity:1;stroke:none;stroke-width:0.24757226px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
                            d="m 32.901261,129.32272 c 0.154196,0.92827 0.730381,0.30721 1.322986,0.60718 0.592605,0.29998 0.655826,0.26636 1.076029,0.12456 0.420203,-0.1418 0.256793,-0.31139 0.784293,-0.31139 0.527499,0 0.675299,0.36842 1.258194,0.17172 0.582895,-0.1967 0.422324,-0.1301 1.079306,-0.54536 0.656981,-0.41526 0.979048,-0.90989 1.276399,-1.61917 0.297352,-0.70927 0.645114,-0.89841 0.43227,-1.89078 -0.212843,-0.99237 -0.102876,-1.29443 -0.662944,-1.76789 -0.560073,-0.47345 -0.415595,-0.92533 -0.796901,-1.31136 -0.381307,-0.38603 -0.743084,-0.30786 -1.217016,-0.66145 -0.47393,-0.35359 -1.293951,-0.52227 -2.006189,-0.46081 -0.712238,0.0615 -1.378467,0.40747 -1.892628,0.91212 -0.51416,0.50465 -0.632661,0.0951 -1.351102,0.82246 -0.718441,0.72734 -0.301195,0.93216 -0.486829,1.74215 -0.185634,0.81 -0.664715,1.0274 -0.307567,1.68143 0.357149,0.65404 -0.02829,0.79616 0.445972,1.4012 0.474259,0.60504 0.891531,0.17711 1.045727,1.10539 z"
                            id="path7229"
                        />
                        </g>
                    </g>
                    </g>
                </g>
                </svg>
        </div>
    ';
    echo $distribuicao;
}
?>

<!DOCTYPE html>
<html lang=pt-BR>

<head>
    <meta charset=utf-8>
    <meta http-equiv=X-UA-Compatible content="IE=edge">
    <meta name=viewport content="width=device-width,initial-scale=1">
    <meta http-equiv=pragma content=no-cache>
    <!--[if IE]><link rel="icon" href="/favicon.ico"><![endif]-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel=stylesheet>
    <title>Machado</title>
    <link href=/css/app.a5e429b7.css rel=preload as=style>
    <link href=/css/chunk-vendors.649c4d5c.css rel=preload as=style>
    <link href=/css/chunk-vendors.649c4d5c.css rel=stylesheet>
    <link href=/css/app.a5e429b7.css rel=stylesheet>
    <link rel=icon type=image/png sizes=32x32 href=/img/icons/favicon-32x32.png> 
    <link rel=icon type=image/png sizes=16x16 href=/img/icons/favicon-16x16.png> 
    <link rel=manifest href=/manifest.json crossorigin=anonymous>
    <meta name=theme-color content=#4DBA87>
    <meta name=apple-mobile-web-app-capable content=no>
    <meta name=apple-mobile-web-app-status-bar-style content=default>
    <meta name=apple-mobile-web-app-title content=Machado>
    <link rel=apple-touch-icon href=/img/icons/apple-touch-icon-152x152.png> 
    <link rel=mask-icon href=/img/icons/safari-pinned-tab.svg color=#4DBA87>
    <meta name=msapplication-TileImage content=/img/icons/msapplication-icon-144x144.png>
    <meta name=msapplication-TileColor content=#000000>
</head>

<body>
    <div class="container">
    <?php
        Caixa("ETAS", "", "300", "854", "934", "200", "0", "yellow lighten-5", "-1");
        Caixa("CAPTACAO", "CAPTAÇÃO RIO MACHADO", "161", "60", "178", "77", "90", "", "1");
        Caixa("ETA2", "ETA2", "158", "233", "134", "175", "0", "", "1");

        foreach($pontos as $ponto)
        {
            switch($ponto['type'])
            {
                case 'TANQUE':
                case 'CONTATO':
                   Reservatorio($ponto['id'], $ponto['max_reservatorio'], $ponto['value'], $ponto['nome_ponto'], $ponto['sup_top'],
                    $ponto['sup_left'], $ponto['width'], $ponto['height'], $ponto['sup_angle'], $ponto['type']);
                break;
                case 'BOMBA':
                    Bomba($ponto['id'], $ponto['max_reservatorio'], $ponto['value'], $ponto['nome_ponto'], $ponto['sup_top'],
                     $ponto['sup_left'], $ponto['width'], $ponto['height'], $ponto['sup_angle'], $ponto['type']);
                break;
                case 'BOMBA_POCO':
                    BombaPoco($ponto['id'], $ponto['max_reservatorio'], $ponto['value'], $ponto['nome_ponto'], $ponto['sup_top'],
                     $ponto['sup_left'], $ponto['width'], $ponto['height'], $ponto['sup_angle'], $ponto['type']);
                break;
                case 'CANO':
                    Cano($ponto['id'], $ponto['max_reservatorio'], $ponto['value'], $ponto['nome_ponto'], $ponto['sup_top'],
                     $ponto['sup_left'], $ponto['width'], $ponto['height'], $ponto['sup_angle'], $ponto['type']);
                break;
                case 'JUNCAOL':
                case 'JUNCAOT':
                    Juncao($ponto['id'], $ponto['max_reservatorio'], $ponto['value'], $ponto['nome_ponto'], $ponto['sup_top'],
                     $ponto['sup_left'], $ponto['width'], $ponto['height'], $ponto['sup_angle'], $ponto['type']);
                break;
                case 'DISTRIBUICAO':
                    Distribuicao($ponto['id'], $ponto['max_reservatorio'], $ponto['value'], $ponto['nome_ponto'], $ponto['sup_top'],
                     $ponto['sup_left'], $ponto['width'], $ponto['height'], $ponto['sup_angle'], $ponto['type']);
                break;
            }
        }
    ?>
    </div>
</body> 
<script>
    document.addEventListener('DOMContentLoaded', function () {
        M.AutoInit();
    });
</script>

</html>