import { http } from "./config";
import { stringify } from "querystring";

export default {
  listar: tabControl => {
    return http.get(`Supervisorio.php?tipo=${tabControl}`);
  },
  salvarPosicao: (id, drr) => {
    if (process.env.NODE_ENV != "production") {
      return http.post(
        "Update.php",
        stringify({
          password: "@S@g@",
          database: "sagamedi_sats_machado",
          table: "supervisorio",
          clmnValues: `sup_top=${drr.y}, sup_left=${drr.x}`,
          where: `id=${id}`
        })
      );
    }
  },
  salvarTamanho: (id, drr) => {
    if (process.env.NODE_ENV != "production") {
      return http.post(
        "Update.php",
        stringify({
          password: "@S@g@",
          database: "sagamedi_sats_machado",
          table: "supervisorio",
          clmnValues: `height=${drr.h}, width=${drr.w}`,
          where: `id=${id}`
        })
      );
    }
  },
  salvarAngulo: (id, drr) => {
    if (process.env.NODE_ENV != "production") {
      return http.post(
        "Update.php",
        stringify({
          password: "@S@g@",
          database: "sagamedi_sats_machado",
          table: "supervisorio",
          clmnValues: `sup_angle=${drr.angle.toFixed(0)}`,
          where: `id=${id}`
        })
      );
    }
  }
};
