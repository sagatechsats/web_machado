import axios from "axios";

export var http = axios.create({
  withCredentials: false,
  baseURL: "http://servermachado.softether.net:4040/",
  headers: {
    "Access-Control-Allow-Origin": "*",
    "Content-Type": "application/x-www-form-urlencoded"
  }
});
