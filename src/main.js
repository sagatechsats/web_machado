import Vue from "vue";
import App from "./App.vue";
import NavBar from "./components/Nav.vue";
// import NavFooter from "./components/NavFooter.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import "materialize-css";
import "materialize-css/dist/css/materialize.css";

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(NavBar)
}).$mount("#nav");

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
// new Vue({
//   render: h => h(NavFooter)
// }).$mount("#footer");
