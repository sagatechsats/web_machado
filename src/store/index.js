import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    time: Date
  },
  getters: {
    getTime: state => {
      return state.time;
    }
  },
  mutations: {
    setTime(stat, timeNow) {
      stat.time = timeNow;
    }
  },
  actions: {},
  modules: {}
});
