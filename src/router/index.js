import Vue from "vue";
import VueRouter from "vue-router";
import Main from "../views/Main.vue";
import Distritos from "../views/Distritos.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "main",
    component: Main
  },
  {
    path: "/distritos",
    name: "distritos",
    component: Distritos
  }
];

const router = new VueRouter({
  routes,
  mode: "history"
});

export default router;
